package au.gov.defence.drm.dehsPreProcessor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used to connect to the PERSEC schema (SQL dataabse)
 * @author Robert Whittaker
 *
 */
public class DbConnector implements AutoCloseable {
    private static final Logger logger =
            LoggerFactory.getLogger(FileAnalyser.class);

    private Connection connection = null;
    private String host = null;
    private String port = null;
    private String schema_name = null;
    private String sid = null;
    private String password = null;

    /**
     * Creates a new DbConnector instance
     * @param config - the configuration class
     * @throws SQLException - when a connection cannot be made to the database
     */
    public DbConnector(AppConfig config) throws SQLException {
        this.host = config.server_name;
        this.port = config.port;
        this.schema_name = config.schema_name;
        this.sid = config.sid;
        this.password = config.password;

        this.connection = getNewConnection();
    }

    /**
     * Creates a connection string, such that the database can be accessed
     * @param schema_name - the schema name of the database
     * @param password - the password required to access the database
     * @param host - the host of the database
     * @param port - the port number required to access the data
     * @param sid - the sid of the database
     * @return String - the connection string that is required to access the database
     */
    private String makeConnectionString(String schema_name, String password, String host, String port, String sid) {
        return this.schema_name + "/" + this.password + "@" + host + ":" + port + ":" + sid;
    }


    /**
     * Creates the connection between the application and the database server.
     * @returns Connection - the connection to the database
     * @throws SQLException - when the database cannot be connected to
     */
     private Connection getNewConnection() throws SQLException {
        String connStr = makeConnectionString(this.schema_name, this.password, this.host, this.port, this.sid);
        Connection conn;
        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            conn = DriverManager.getConnection("jdbc:oracle:thin:" + connStr, this.schema_name, this.password);
            conn.setAutoCommit(false);
            logger.debug("SQL Connection established:" + host + "/" + schema_name);
        } catch (SQLException e) {
            String errm = "Trying to open SQL connection to: " + host + "/" + schema_name;
            logger.error(errm, e);
            throw e;
        }
        return conn;
    }

    @Override
    public void close() throws SQLException  {
        logger.debug("Close SQL Connection:" + host + "/" + schema_name);
        connection.close();
    }

    /**
     * This class rechecks to see if the connection with the database has not been disconnected, if not it reconnects.
     * @return Connection - the connection to the database.
     * SQL statements are executed and results are returned within the context of a connection.
     * @throws SQLException - if the database cannot be connected to
     */
    public Connection getConnection() throws SQLException {
        // Added code to check if the connection is closed, If so it attempts a new conection
        if (this.connection.isClosed()) {
            this.connection = getNewConnection();
        }
        return this.connection;
    }

}
