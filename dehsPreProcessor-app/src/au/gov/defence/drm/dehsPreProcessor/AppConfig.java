package au.gov.defence.drm.dehsPreProcessor;

import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.dataformat.javaprop.JavaPropsFactory;

import au.gov.defence.drm.centralconf.DrmCentralConfigProperties;
import au.gov.defence.drm.doji.sql.SimpleCrypt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonMerge;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Rob Whittaker
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppConfig {

    @JsonProperty(value = "server_name", required = true)
    String server_name;

    @JsonProperty(value = "port", required = true)
    String port;

    @JsonProperty(value = "sid", required = true)
    String sid;

    @JsonProperty(value = "schema_name", required = true)
    String schema_name;

    @JsonProperty(value = "password", required = true)
    String password;

    @JsonProperty(value = "source", required = true)
    String source;

    @JsonProperty(value = "failures", required = true)
    String failures;

    @JsonProperty(value = "timebetweenfolderchecks", required = true)
    Integer timebetweenfolderchecks;

    @JsonProperty(value = "objective_permission_user_id", required = true)
    String objective_permission_user_id;

    @JsonProperty(value = "objective_email_to_id", required = true)
    String objective_email_to_id;

    @JsonProperty(value = "objective_email_from_id", required = true)
    String objective_email_from_id;

    @JsonProperty(value = "army_nonserving", required = true)
    String army_nonserving;

    @JsonProperty(value = "army_serving", required = true)
    String army_serving;

    @JsonProperty(value = "navy_nonserving", required = true)
    String navy_nonserving;

    @JsonProperty(value = "navy_serving", required = true)
    String navy_serving;

    @JsonProperty(value = "raaf_nonserving", required = true)
    String raaf_nonserving;

    @JsonProperty(value = "raaf_serving", required = true)
    String raaf_serving;

    @JsonProperty(value = "forms_nonserving", required = true)
    List<String> forms_nonserving;

    @JsonProperty(value = "orderPmkeys", required = true)
    Integer orderPmkeys;

    @JsonProperty(value = "orderSurname", required = true)
    Integer orderSurname;

    @JsonProperty(value = "orderDocumentCode", required = true)
    Integer orderDocumentCode;

    @JsonProperty(value = "throughputPerTime", required = true)
    Integer throughputPerTime;

    @JsonProperty(value = "maxDocOutputFolder", required = true)
    Integer maxDocOutputFolder;

    @JsonProperty(value = "progressReportPath", required = true)
    String progressReportPath;

    // Doji
    @JsonProperty(value = "doji", required = false)
    @JsonMerge
    DojiConfig doji;

    public AppConfig() {
        // Empty Constructor
    }
    
    public AppConfig(DojiConfig doji) {
        this.doji = doji;
    }

    public Integer getThroughputPerTime() {
        return throughputPerTime;
    }

    public void setThroughputPerTime(Integer throughputPerTime) {
        this.throughputPerTime = throughputPerTime;
    }

    public Integer getMaxDocOutputFolder() {
        return maxDocOutputFolder;
    }

    public void setMaxDocOutputFolder(Integer maxDocOutputFolder) {
        this.maxDocOutputFolder = maxDocOutputFolder;
    }

    public String getProgressReportPath() {
        return progressReportPath;
    }

    public void setProgressReportPath(String progressReportPath) {
        this.progressReportPath = progressReportPath;
    }

    // Getters and Setters
    public String getServer_name() {
        return server_name;
    }

    public void setServer_name(String server_name) {
        this.server_name = server_name;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSchema_name() {
        return schema_name;
    }

    public void setSchema_name(String schema_name) {
        this.schema_name = schema_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = SimpleCrypt.rmsDecrypt(password, SimpleCrypt.getDefaultPassKey());
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFailures() {
        return failures;
    }

    public void setFailures(String failures) {
        this.failures = failures;
    }

    public Integer getTimebetweenfolderchecks() {
        return timebetweenfolderchecks;
    }

    public void setTimebetweenfolderchecks(Integer timebetweenfolderchecks) {
        this.timebetweenfolderchecks = timebetweenfolderchecks;
    }

    public String getObjective_permission_user_id() {
        return objective_permission_user_id;
    }

    public void setObjective_permission_user_id(String objective_permission_user_id) {
        this.objective_permission_user_id = objective_permission_user_id;
    }

    public String getObjective_email_to_id() {
        return objective_email_to_id;
    }

    public void setObjective_email_to_id(String objective_email_to_id) {
        this.objective_email_to_id = objective_email_to_id;
    }

    public String getObjective_email_from_id() {
        return objective_email_from_id;
    }

    public void setObjective_email_from_id(String objective_email_from_id) {
        this.objective_email_from_id = objective_email_from_id;
    }

    public String getArmy_nonserving() {
        return army_nonserving;
    }

    public void setArmy_nonserving(String army_nonserving) {
        this.army_nonserving = army_nonserving;
    }

    public String getArmy_serving() {
        return army_serving;
    }

    public void setArmy_serving(String army_serving) {
        this.army_serving = army_serving;
    }

    public String getNavy_nonserving() {
        return navy_nonserving;
    }

    public void setNavy_nonserving(String navy_nonserving) {
        this.navy_nonserving = navy_nonserving;
    }

    public String getNavy_serving() {
        return navy_serving;
    }

    public void setNavy_serving(String navy_serving) {
        this.navy_serving = navy_serving;
    }

    public String getRaaf_nonserving() {
        return raaf_nonserving;
    }

    public void setRaaf_nonserving(String raaf_nonserving) {
        this.raaf_nonserving = raaf_nonserving;
    }

    public Integer getOrderPmkeys() {
        return orderPmkeys;
    }

    public void setOrderPmkeys(Integer orderPmkeys) {
        this.orderPmkeys = orderPmkeys;
    }

    public Integer getOrderSurname() {
        return orderSurname;
    }

    public void setOrderSurname(Integer orderSurname) {
        this.orderSurname = orderSurname;
    }

    public Integer getOrderDocumentCode() {
        return orderDocumentCode;
    }

    public void setOrderDocumentCode(Integer orderDocumentCode) {
        this.orderDocumentCode = orderDocumentCode;
    }

    public String getRaaf_serving() {
        return raaf_serving;
    }

    public void setRaaf_serving(String raaf_serving) {
        this.raaf_serving = raaf_serving;
    }

    public List<String> getForms_nonserving() {
        return forms_nonserving;
    }

    public void setForms_nonserving(List<String> forms_nonserving) {
        this.forms_nonserving = forms_nonserving;
    }

    public DojiConfig getDoji() {
        return doji;
    }

    public void setDoji(DojiConfig doji) {
        this.doji = doji;
    }

    public static AppConfig load(final Path path) throws IOException {

        final ObjectMapper mapper = new ObjectMapper(new JavaPropsFactory());

        // Find and load the Central config properties files into our class wrapper.
        CentralConfig centralConfig = CentralConfig.load();
        AppConfig tmp = new AppConfig(centralConfig.dojiConf);
                
        
        // We read File into Properties, then write it back to a String, 
        // so we can parse it with the ObjectMapper. 
        // Very inefficient.. we do this cause "DrmCentralConfigProperties.class" does not 
        // expose the path to the file.
        final StringWriter writer = new StringWriter();
        centralConfig.props.store(writer, null);
        System.out.println(writer);
        tmp = mapper.readerForUpdating(tmp).readValue(writer.toString());
        		
        return mapper.readerForUpdating(tmp).readValue(path.toFile());

    }

}
