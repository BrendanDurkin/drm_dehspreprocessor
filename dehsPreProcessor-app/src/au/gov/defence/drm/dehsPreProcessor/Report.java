package au.gov.defence.drm.dehsPreProcessor;

import java.util.Date;

public class Report {

    public String timeSnapshot;
    public long epochTimeMillis;
    public int succesfullyMovedDocumentsLast5Mins;
    public int succesfullyMovedDocumentsLast15Mins;
    public int succesfullyMovedDocumentsLast60Mins;
    public int succesfullyMovedDocumentsLastDay;
    public int failedDocumentsLast5Mins;
    public int failedDocumentsLast15Mins;
    public int failedDocumentsLast60Mins;
    public int failedDocumentsLastDay;

    public Report(long millis, int succesfullyMovedDocumentsLast5Mins, int succesfullyMovedDocumentsLast15Mins, int succesfullyMovedDocumentsLast60Mins, int succesfullyMovedDocumentsLastDay,
            int failedDocumentsLast5Mins, int failedDocumentsLast15Mins, int failedDocumentsLast60Mins, int failedDocumentsLastDay) {
        this.timeSnapshot = (new Date(millis).toString());
        this.epochTimeMillis = millis;
        this.succesfullyMovedDocumentsLast5Mins = succesfullyMovedDocumentsLast5Mins;
        this.succesfullyMovedDocumentsLast15Mins = succesfullyMovedDocumentsLast15Mins;
        this.succesfullyMovedDocumentsLast60Mins = succesfullyMovedDocumentsLast60Mins;
        this.succesfullyMovedDocumentsLastDay = succesfullyMovedDocumentsLastDay;
        this.failedDocumentsLast5Mins = failedDocumentsLast5Mins;
        this.failedDocumentsLast15Mins = failedDocumentsLast15Mins;
        this.failedDocumentsLast60Mins = failedDocumentsLast60Mins;
        this.failedDocumentsLastDay = failedDocumentsLastDay;

    }
}
