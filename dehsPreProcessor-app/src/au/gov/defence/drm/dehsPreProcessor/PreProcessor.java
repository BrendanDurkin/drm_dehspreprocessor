package au.gov.defence.drm.dehsPreProcessor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import picocli.CommandLine;
import picocli.CommandLine.Option;

/**
 * @author Robert Whittaker
 *
 */
public class PreProcessor implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(PreProcessor.class);

    public PreProcessor() {
        // Empty constructor.
    }

    @Option(names = { "-c",
            "--config" }, required = true, description = "The location of the properties file.")
    private String config;

    @Option(names = { "--help" }, usageHelp = true, description = "Display a help message.")
    private Boolean help = false;

    public static void main(final String[] args) {
        CommandLine.run(new PreProcessor(), System.err, args);
    }

    @Override
    public void run() {
        final CompletableFuture<Void> computation = CompletableFuture.runAsync(this::mainloop);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> stop(computation)));

        computation.join();

    }

    private void mainloop() {
        try {
            new Monitor(AppConfig.load(Paths.get(this.config)), false);
        } catch (FileNotFoundException e) {
            logger.error("The properties file is not found", e);
        } catch (InterruptedException e) {
            logger.error("Application Terminating");
        } catch (IOException e) {
            logger.error("Error within the Properties file", e);
        }
    };

    private void stop(final Future<Void> computation) {
        logger.warn("Stopping the service.");
        computation.cancel(true);
    }
}