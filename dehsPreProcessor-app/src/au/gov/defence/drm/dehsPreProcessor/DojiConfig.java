package au.gov.defence.drm.dehsPreProcessor;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Properties;
import au.gov.defence.drm.doji.config.Config;

//@Immutable
public class DojiConfig {


    @JsonProperty(value = "dojiSchema", required = false)
    public  String schema;

    @JsonProperty(value = "dojiPassword", required = false)
    public  String password;

    @JsonProperty(value = "server", required = false)
    public  String server;

    @JsonProperty(value = "sid", required = false)
    public  String sid;

    @JsonProperty(value = "readPassword", required = false)
    public  String readPassword;

    @JsonProperty(value = "folderCacheAge", required = false)
    public  Integer folderCacheAge;

    @JsonProperty(value = "documentCacheAge", required = false)
    public  Integer documentCacheAge;

    @JsonProperty(value = "locationCacheAge", required = false)
    public  Integer locationCacheAge;

    @JsonProperty(value = "fileCacheAge", required = false)
    public  Integer fileCacheAge;

    @JsonProperty(value = "profilingInterval", defaultValue = "5000")
    public  Integer profilingInterval;

    public DojiConfig() { }

    public DojiConfig(
        @JsonProperty(value = "dojiSchema", required = false)
        final String schema,

        @JsonProperty(value = "dojiPassword", required = false)
        final String password,

        @JsonProperty(value = "server", required = false)
        final String server,

        @JsonProperty(value = "sid", required = false)
        final String sid,

        @JsonProperty(value = "readPassword", required = false)
        final String readPassword,

        @JsonProperty(value = "folderCacheAge", required = false)
        final int folderCacheAge,

        @JsonProperty(value = "documentCacheAge", required = false)
        final int documentCacheAge,

        @JsonProperty(value = "locationCacheAge", required = false)
        final int locationCacheAge,

        @JsonProperty(value = "fileCacheAge", required = false)
        final int fileCacheAge,

        // For some reason Jackson isn't picking up the default
        // value. I suspect that's a bug in Jackson. Not
        // a big deal in this instance.
        @JsonProperty(value = "profilingInterval", defaultValue = "5000")
        final int profilingInterval
    ) {
        this.schema            = schema;
        this.password          = password;
        this.server            = server;
        this.sid               = sid;
        this.readPassword      = readPassword;

        this.folderCacheAge    = folderCacheAge;
        this.documentCacheAge  = documentCacheAge;
        this.locationCacheAge  = locationCacheAge;
        this.fileCacheAge      = fileCacheAge;
        this.profilingInterval = profilingInterval;

    }

    public Properties getProperties() {

        final Properties props = new Properties();

        props.setProperty(Config.DOJI_SCHEMA,   schema);
        props.setProperty(Config.DOJI_PASSWORD, password);
        props.setProperty(Config.SERVER,        server);
        props.setProperty(Config.SID,           sid);
        props.setProperty(Config.READ_PASSWORD, readPassword);

        props.setProperty(
            Config.FOLDER_CACHE_AGE, folderCacheAge.toString()
        );

        props.setProperty(
            Config.DOCUMENT_CACHE_AGE, documentCacheAge.toString()
        );

        props.setProperty(
            Config.LOCATION_CACHE_AGE, locationCacheAge.toString()
        );

        props.setProperty(
            Config.FILE_CACHE_AGE, fileCacheAge.toString()
        );

        props.setProperty(
            Config.PROFILING_INTERVAL, profilingInterval.toString()
        );

        return props;
    }

}
