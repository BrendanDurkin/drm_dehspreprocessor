package au.gov.defence.drm.dehsPreProcessor;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;

import java.nio.file.Paths;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import au.gov.defence.drm.doji.Doji;
import au.gov.defence.drm.doji.ObjContext;
import au.gov.defence.drm.doji.objects.ObjUser;

/**
 * @author Robert Whittaker
 *
 */
public class Monitor {

	private static final Logger logger = LoggerFactory.getLogger(Monitor.class);
	public static Boolean isRunning = true;

	public static CopyOnWriteArrayList<Long> successfulResultsList = new CopyOnWriteArrayList<Long>();
	public static CopyOnWriteArrayList<Long> failedResultsList = new CopyOnWriteArrayList<Long>();
	public static File statsFile;

	AppConfig config;
	Properties dojiProperties;

	/**
	 * Creates the monitoring class Loads the config and sets up the DOJI properties
	 * file
	 *
	 * @param configurationFile The path to the configuration file
	 * @param test              - whether it is a unit test or not, that only wants
	 *                          to loop though once
	 * @throws InterruptedException - when the application has been told to stop
	 */
	public Monitor(AppConfig configurationFile, Boolean test) throws InterruptedException {

		this.config = configurationFile;

		logger.debug("Setting up Doji Properties File");
		// Setup doxi properties file
		this.dojiProperties = this.config.doji.getProperties();

		// this.dojiProperties.load();

		Boolean check = confiurationChecker();

		if (check) {
			statsFile = new File(this.config.progressReportPath);

			// Does not run the monitor thread if in testing
			if (!test) {
				new Statistics().start();
			}

			logger.info("Starting folder observer.");

			do {
				observer();
			} while (!test && isRunning);
		}

	}

	// Checks to see if the configurations have valid inputs
	/**
	 * Checks the configuration file to see if all the checks have passed at the
	 * start of the application
	 *
	 * @return Boolean - true if all checks passed, false if a test failed
	 */
	private Boolean confiurationChecker() {

		String version = this.getClass().getPackage().getImplementationVersion();
		// By collecting status along the way we do more tests
		Boolean configPassed = true; // set a default true and wait to fail

		logger.info("Version: " + version);
		
		logger.info("Checking the configuration values...");
		logger.info("Doji Values");
		logger.info("Doji Schema: " + this.dojiProperties.getProperty("dojiSchema"));
		logger.info("Doji server: " + this.dojiProperties.getProperty("server"));
		logger.info("Doji sid: " + this.dojiProperties.getProperty("sid"));
		logger.info("Doji folderCacheAge: " + this.dojiProperties.getProperty("folderCacheAge"));
		logger.info("Doji documentCacheAge: " + this.dojiProperties.getProperty("documentCacheAge"));
		logger.info("Doji locationCacheAge: " + this.dojiProperties.getProperty("locationCacheAge"));
		logger.info("Doji fileCacheAge: " + this.dojiProperties.getProperty("fileCacheAge"));
		
		logger.info("Email Values");
		logger.info("objective_permission_user_id: " + this.config.objective_permission_user_id);
		logger.info("objective_email_to_id: " + this.config.objective_email_to_id);
		logger.info("objective_email_from_id: " + this.config.objective_email_from_id);
				
		// Try to connect to doji
		logger.info("Trying to connect to DOJI...");

		try (ObjContext doji = Doji.newContext(this.dojiProperties)) {

			doji.setAutoCommit(false);
			logger.info("Connection to DOJI successful.");

			// Check to see if email Id's are valid
			logger.info("Checking email ID's...");

			try {
				ObjUser admin = doji.getUser(config.objective_permission_user_id);
				try {
					doji.getUser(config.objective_email_from_id);
				} catch (Exception e) {
					logger.warn("Invalid Email from ID", e);
					configPassed = false;
				}
				try {
					doji.getObject(config.objective_email_to_id, admin);
				} catch (Exception e) {
					logger.warn("Invalid Email to ID", e);
					configPassed = false;
				}

			} catch (Exception e) {
				logger.warn("Invalid Email admin ID", e);
				configPassed = false;
			}

			logger.info("Email ID's valid.");

		} catch (Exception e) {
			logger.error("Invalid DOJI Details", e);
			configPassed = false;
		}

		// Checking folder locations
		logger.info("Folders");
		logger.info("Source Folder: " + this.config.source);
		logger.info("Failure Folder: " + this.config.failures);
		logger.info("Army Non Serving Folder: " + this.config.army_nonserving);
		logger.info("Army Serving Folder: " + this.config.army_serving);
		logger.info("Navy Non Serving Folder: " + this.config.navy_nonserving);
		logger.info("Navy Serving Folder: " + this.config.navy_serving);
		logger.info("Raaf Non Serving Folder: " + this.config.raaf_nonserving);
		logger.info("Raaf Serving Folder: " + this.config.raaf_serving);
		
		
		logger.info("Checking Folder Locations...");
		try {
			if (!Files.exists(Paths.get(this.config.source))) {
				logger.error("Source folder is in an invalid location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("source does not exist in properties file");
		}
		try {
			if (!Files.exists(Paths.get(this.config.failures))) {
				logger.error("Failures fldoer is in an Invalid location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("failures does not exist in properties file");
		}

		try {
			if (!Files.exists(Paths.get(this.config.army_nonserving))) {
				logger.error("Army nonserving folder is in an Invalid Location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("army_nonserving does not exist in properties file");
		}
		try {
			if (!Files.exists(Paths.get(this.config.army_serving))) {
				logger.error("Army serving folder is in an Invalid Location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("army_serving does not exist in properties file");
		}

		try {
			if (!Files.exists(Paths.get(this.config.navy_nonserving))) {
				logger.error("Navy nonserving folder is in an Invalid Location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("navy_nonserving does not exist in properties file");
		}

		try {
			if (!Files.exists(Paths.get(this.config.navy_serving))) {
				logger.error("Navy serving folder is in an Invalid Location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("navy_serving does not exist in properties file");
		}

		try {
			if (!Files.exists(Paths.get(this.config.raaf_nonserving))) {
				logger.error("RAAF nonserving folder is in an Invalid Location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("raaf_nonserving does not exist in properties file");
		}

		try {
			if (!Files.exists(Paths.get(this.config.raaf_serving))) {
				logger.error("RAAF serving folder is in an Invalid Location");
				configPassed = false;
			}
		} catch (NullPointerException e) {
			configPassed = false;
			logger.error("raaf_nonserving does not exist in properties file");
		}
		logger.info("Folder locations checked.");

		if (this.config.forms_nonserving == null) {
			this.config.forms_nonserving = new ArrayList<String>();
		}
		// Checking Non-Serving Forms
		logger.info("Non Serving Forms");
		for(int i = 0; i < this.config.forms_nonserving.size(); i++) {
			logger.info("Non Serving Form " + i + " is: " + this.config.forms_nonserving.get(i));
		}
		
		// PERSEC Serving information
		logger.info("Database server");
		logger.info("Server Name: " + this.config.server_name);
		logger.info("Server port: " + this.config.port);
		logger.info("Server SID: " + this.config.sid);
		logger.info("Server Schema Name: " + this.config.schema_name);
	
		
		// Try to connect to database server
		logger.info("Trying to connect to database server...");

		try (Connection con = new DbConnector(config).getConnection()) {
			// Connection con = new DbConnector(config).getConnection();
			logger.info("Connected to database server.");
		} catch (SQLException e) {
			logger.error("Failed to connect to database server.");
			configPassed = false;
		}

		// Check xmlFile
		logger.info("Checking Prgoress XML Report File");
		logger.info("Process Report: " + this.config.progressReportPath);
		if (this.config.progressReportPath == null) {
			configPassed = false;
			logger.error("progressReportPath does not exist in properties file");
		}

		logger.info("Checking file name positioning");
		if (this.config.orderDocumentCode == null) {
			configPassed = false;
			logger.error("orderDocumentCode does not exist in properties file");
		}
		logger.info("orderDocumentCode: " + this.config.orderDocumentCode);

		if (this.config.orderPmkeys == null) {
			configPassed = false;
			logger.error("orderPmkeys does not exist in properties file");
		}
		logger.info("orderPmkeys: " + this.config.orderPmkeys);

		if (this.config.orderSurname == null) {
			configPassed = false;
			logger.error("orderSurname does not exist in properties file");
		}
		logger.info("orderSurname: " + this.config.orderSurname);

		// Check times
		
		logger.info("Throughput limitations");
		logger.info("throughputPerTime: " + this.config.throughputPerTime);
		logger.info("timebetweenfolderchecks: " + this.config.timebetweenfolderchecks);
		logger.info("maxDocOutputFolder: " + this.config.maxDocOutputFolder);
		
		if (this.config.throughputPerTime == null) {
			configPassed = false;
			logger.error("throughputPerTime does not exist in properties file");
		}

		if (this.config.timebetweenfolderchecks == null) {
			configPassed = false;
			logger.error("timebetweenfolderchecks does not exist in properties file");
		}

		if (this.config.maxDocOutputFolder == null) {
			configPassed = false;
			logger.error("maxDocOutputFolder does not exist in properties file");
		}

		return configPassed;

	}

	// observes the folder
	/**
	 * Observes a folder to detect when files have been added to it. When a file is
	 * detected it is then sent to be analysed
	 *
	 * @author Robert Whittaker
	 *
	 * @throws InterruptedException - When the program has been told to stop
	 */
	public void observer() throws InterruptedException {

		final FileAnalyser fileAnalyser = new FileAnalyser(this.config, this.dojiProperties);

		// Gets the source folder
		final File sourceFolder = new File(this.config.getSource());

		try {

			// For Files originally in source folder

			File[] fileList = sourceFolder.listFiles();
			if (fileList.length > 0) {
				try (DbConnector databaseConnector = new DbConnector(config)) {

					try (Connection connection = databaseConnector.getConnection()) {
						logger.trace("Database Connection Established");
						// due to maximum number of querys that can be done on a connection counter is
						// needed

						for (File file : fileList) {
							Boolean canContinue = checkMaxiumDocsPerOutputFolder(this.config);
							if (canContinue) {
								String outputFolderName = fileAnalyser.analyse(file, connection);
								if (outputFolderName.equals(this.config.failures) || outputFolderName.equals(this.config.source)) {
									failedResultsList.add(System.currentTimeMillis());
								} else {
									logger.info(file.getName() + " has successfully been moved to " + outputFolderName);
									successfulResultsList.add(System.currentTimeMillis());
								}
							} else {
								logger.trace(
										"The maximum number of files in the output folder has been found, as such folder loop has bee broken");
								// Waits until it can progress
								while (!checkMaxiumDocsPerOutputFolder(this.config)) {
									// sleeps for 20 milliseconds before retrying
									Thread.sleep(this.config.throughputPerTime);
								}

							}
						}

					}
				}
				
			} else {
				Thread.sleep(this.config.timebetweenfolderchecks);
			}

		} catch (InterruptedException e) {
			isRunning = false;
			throw e;
		} catch (SQLException e) {
			logger.error("A connection cannot be made to the PERSEC database", e);
		}

	}

	/**
	 * Checks whether the number of documents within the output folders has not
	 * reached its limit as specifed within the YAML file.
	 *
	 * @param conf - The App Config of the application
	 * @return true - if the count is under the limit, false if the count is equal
	 *         or greater than the limit
	 */
	public Boolean checkMaxiumDocsPerOutputFolder(AppConfig conf) {

		Integer totalNumberOfFilesInOutputFolders = 0;
		totalNumberOfFilesInOutputFolders = totalNumberOfFilesInOutputFolders
				+ docsInOutputFolder(new File(conf.army_nonserving));
		totalNumberOfFilesInOutputFolders = totalNumberOfFilesInOutputFolders
				+ docsInOutputFolder(new File(conf.army_serving));
		totalNumberOfFilesInOutputFolders = totalNumberOfFilesInOutputFolders
				+ docsInOutputFolder(new File(conf.raaf_nonserving));
		totalNumberOfFilesInOutputFolders = totalNumberOfFilesInOutputFolders
				+ docsInOutputFolder(new File(conf.raaf_serving));
		totalNumberOfFilesInOutputFolders = totalNumberOfFilesInOutputFolders
				+ docsInOutputFolder(new File(conf.navy_nonserving));
		totalNumberOfFilesInOutputFolders = totalNumberOfFilesInOutputFolders
				+ docsInOutputFolder(new File(conf.navy_serving));

		return (totalNumberOfFilesInOutputFolders < conf.maxDocOutputFolder);
	}

	/**
	 * Counts the number of documents within the output folders
	 *
	 * @param folder - Folder to be analysed
	 * @return - The number of documents within the folder
	 */
	public static Integer docsInOutputFolder(File folder) {
		return (folder.listFiles().length);
	}


	public class Statistics extends Thread {

		@Override
		public void run() {
			try {

				logger.info("Started Statistics Thread");

				XmlMapper xmlMapper = new XmlMapper();

				logger.debug("Created an XMLMapper");
				xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
				logger.debug("Enabled indentation on XML Output");
				

				while (isRunning) {

					long startTime = System.currentTimeMillis();

					// Remove old successful results
					if (successfulResultsList.size() > 0) {
						while (startTime - successfulResultsList.get(0) > 24 * 60 * 60 * 1000) {
							successfulResultsList.remove(0);
						}
					}

					List<Long> sR = successfulResultsList;

					// Remove old failed results
					if (failedResultsList.size() > 0) {
						while (startTime - failedResultsList.get(0) > 24 * 60 * 60 * 1000) {
							failedResultsList.remove(0);
						}
					}

					List<Long> lR = failedResultsList;

					Integer succeedDay = sR.size();
					Integer succeed60 = (int) sR.stream().filter(value -> (startTime - value < 60 * 60 * 1000)).count();
					Integer succeed15 = (int) sR.stream().filter(value -> (startTime - value < 15 * 60 * 1000)).count();
					Integer succeed5 = (int) sR.stream().filter(value -> (startTime - value < 5 * 60 * 1000)).count();

					Integer failedDay = lR.size();
					Integer failed60 = (int) lR.stream().filter(value -> (startTime - value < 60 * 60 * 1000)).count();
					Integer failed15 = (int) lR.stream().filter(value -> (startTime - value < 15 * 60 * 1000)).count();
					Integer failed5 = (int) lR.stream().filter(value -> (startTime - value < 5 * 60 * 1000)).count();

					xmlMapper.writeValue(statsFile, new Report(startTime, succeed5, succeed15, succeed60, succeedDay,
							failed5, failed15, failed60, failedDay));

					logger.trace("Thread has successfully logged monitoring information into XML file");
					

					while (System.currentTimeMillis() - startTime < 60 * 1000) {
						Thread.sleep(1000);
					}

				}

			} catch (InterruptedException e) {
				logger.error("Monitoring thread has been terminated", e);
			} catch (JsonGenerationException e) {
				logger.error("Json generated Exception within monitor", e);
			} catch (JsonMappingException e) {
				logger.error("JSON Mapping Exception within monitor", e);
			} catch (IOException e) {
				logger.error("IOException within monitor", e);
			}
		}

	}

}
