package au.gov.defence.drm.dehsPreProcessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.gov.defence.drm.doji.Doji;
import au.gov.defence.drm.doji.ObjContext;
import au.gov.defence.drm.doji.exceptions.DojiException;
import au.gov.defence.drm.doji.objects.ObjUser;
import au.gov.defence.drm.doji.objects.ObjUserGroup;

/**
 * @author Robert Whittaker
 *
 */
public class FileAnalyser {
	String pmKeys;
	String surname;
	String documentCode;
	String fileName;

	Properties dojiProperties;
	AppConfig config;

	private static final Logger logger = LoggerFactory.getLogger(FileAnalyser.class);

	/**
	 * Creates a constructor of the FileAnalyser class
	 *
	 * @param config         - The configuration class
	 * @param dojiProperties - The properties required for Doji
	 */

	public FileAnalyser(AppConfig config, Properties dojiProperties) {

		this.config = config;
		this.dojiProperties = dojiProperties;
	}

	/**
	 * Analyses a file and returns the desired output folder
	 * 
	 * @param file               - file to be analysed
	 * @param databaseConnection - connection to the database
	 * @return String Path of output folder
	 * @throws InterruptedException
	 */
	public String analyse(File file, Connection databaseConnection) throws InterruptedException {
		pmKeys = null;
		surname = null;
		documentCode = null;
		fileName = file.getName();
		Boolean splitNameSuccessful = splitFileName(fileName);
		final String outputFolder = splitNameSuccessful ? getOutputFolderName(databaseConnection) : this.config.failures;
		logger.debug("Desired Output Folder for: " + this.fileName + " is " + outputFolder);

		if (!moveFile(file, outputFolder)) {
			throw new InterruptedException();
		}
		// Limits throughput
		Thread.sleep(config.throughputPerTime);

		return outputFolder;

	}
	// UMR-<PMKEYS>-<SURNAME>-<DOCUMENT CODE>-EY<Effective Year in YYYY
	// format>-<DOCUMENT DATE in YYYYMMDD format>.PDF

	/**
	 * Splits the filename using the '-' seperator and places it into an array;
	 * 
	 * @return Boolean, whether it was successful in splitting the file name
	 */
	private Boolean splitFileName(String fileName) {
		final String[] splitFileName = fileName.split("-");
		try {
			pmKeys = splitFileName[config.orderPmkeys];
			surname = splitFileName[config.orderSurname];
			documentCode = splitFileName[config.orderDocumentCode];
		} catch (ArrayIndexOutOfBoundsException e) {
			// Invalid File Name
			logger.info("An invalid File name for file: " + fileName + " was found.");
			sendEmail(this.dojiProperties, this.config, "Invalid File Name",
					"An invalid File name for file: " + fileName + " was found. \n" + "Please rename the file. \n");

			return false;
		}
		return true;
	}

	/**
	 * Gets the path of the desired output folder. It connects to the Database and
	 * works out whether the file has a valid name. It then analyses this name to
	 * work out the desired folder.
	 * 
	 * @return String - the path of the desired output folder
	 */

	private String getOutputFolderName(Connection databaseConnection) {

		String outputFolder = this.config.failures;

		try (PreparedStatement preparedStatement = databaseConnection
				.prepareStatement(FIND_SERVICE_FROM_PMKEYS_AND_SURNAME);) {

			logger.info("pmKeys: " + pmKeys);
			preparedStatement.setString(1, pmKeys);
			logger.info("surname: " + surname);
			preparedStatement.setString(2, surname);

			List<String> qryResultStringList = new ArrayList<String>();
			try (ResultSet rs = preparedStatement.executeQuery();){
				
				while (rs.next()) {
					qryResultStringList.add(rs.getString(1));
				}
			}

			// list contains
			Boolean folderNonServingBoolean = false;
			if (config.forms_nonserving.contains(documentCode.toString())) {
				folderNonServingBoolean = true;
			}

			if (qryResultStringList.size() == 1) {
				

				final String service = qryResultStringList.get(0);
				logger.info("service: " + service);
				switch (service) {
				
				case "RAAF":
					outputFolder = folderNonServingBoolean ? config.raaf_nonserving : config.raaf_serving;
					break;
				case "ARMY":
					outputFolder = folderNonServingBoolean ? config.army_nonserving : config.army_serving;
					break;
				case "NAVY":
					outputFolder = folderNonServingBoolean ? config.navy_nonserving : config.navy_serving;
					break;
				default:
					// Invalid service
					logger.info("Invalid Service Found, Service: " + service + ". The Service found for file: "
							+ fileName + " must be RAAF, ARMY or NAVY.");
					sendEmail(dojiProperties, config, "Invalid Service",
							"The Service found for file: " + fileName + " must be RAAF, ARMY or NAVY.");
					break;
				}
			} else {
				try (PreparedStatement preparedStatementPmKeyOnly = databaseConnection
						.prepareStatement(FIND_SERVICE_FROM_PMKEYS);) {
					preparedStatementPmKeyOnly.setString(1, pmKeys);
					// Check if PmKey found but invalid surname
					qryResultStringList.clear();
					try (ResultSet rs = preparedStatementPmKeyOnly.executeQuery();){
						
						while (rs.next()) {
							qryResultStringList.add(rs.getString(1));
						}
					}

					if (qryResultStringList.size() > 0) {
						// PmKeys number does not match surname
						logger.info("PmKeys number does not match surname:" + pmKeys + ":" + surname);
						sendEmail(dojiProperties, config, "PmKeys number does not match surname",
								"The  PmKeys for file: " + fileName + " does not match the surname found. \n");
					} else {
						// Invalid PmKeys Number or Surname
						logger.info("Invalid PmKeys Number or Surname");
						sendEmail(dojiProperties, config, "Invalid PmKeys or Surname",
								"An invalid PmKeys and Surname for file: " + fileName + " was found. \n");
					}
				}
			}

		} catch (SQLException e) {
			// Problem with database configurations
			outputFolder = this.config.source;
			
			logger.warn("There are problems querying the database, will try again", e);

		}
		return outputFolder;

	}

	/**
	 * Sending an email using Doji
	 *
	 * @param props   - The doji properties file
	 * @param config  - A AppConfig class which is specific to this application
	 * @param title   - The title of the email
	 * @param message - The Message of the email
	 */
	public void sendEmail(Properties props, AppConfig config, String title, String message) {

		ObjUser admin;
		ObjUser from;
		ObjUserGroup<?, ?> to;

		// Attempt to create a new Doji context

		try (ObjContext doji = Doji.newContext(props)) {
			logger.trace("New Doji Context Created");

			// Attempt to get user whose permissions are checked against
			try {
				admin = doji.getUser(config.objective_permission_user_id);

			} catch (DojiException e) {
				logger.warn("Failed to get user whose permissions are checked against, DojiException.", e);
				return;
			} catch (SQLException e) {
				logger.warn("Failed to get user whose permissions are checked against, SQLException.", e);
				return;
			}

			// Attempt to get the user whom the email will be from
			try {
				from = doji.getUser(config.objective_email_from_id);
			} catch (DojiException e) {
				logger.warn("Failed to get user whose email it is sent from, DojiException.", e);
				return;
			} catch (SQLException e) {
				logger.warn("Failed to get user whose email it is sent from, SQLException.", e);
				return;
			}

			// Attempt to get the user or group whose email it will be sent to
			try {
				to = doji.getObject(config.objective_email_to_id, admin);
			} catch (DojiException e) {
				logger.warn("Failed to get users whose email it is sent to, DojiException.", e);
				return;
			} catch (SQLException e) {
				logger.warn("Failed to get users whose email it is sent to, SQLException.", e);
				return;
			}

			// Attempt to send the email
			try {
				doji.sendEmail(title, message, null, from, to);
			} catch (DojiException e) {
				logger.warn("Failed to send the email, DojiException.", e);
				return;
			} catch (SQLException e) {
				logger.warn("Failed to send the email, SQLException.", e);
				return;
			}

			// Attempt to commit in Doji
			try {
				doji.commit();
			} catch (DojiException e) {
				logger.warn("Failed to commit to Doji, DojiException.", e);
				return;
			} catch (SQLException e) {
				logger.warn("Failed to commit to Doji, SQLException.", e);
				return;
			}

		} catch (DojiException e) {
			logger.warn("Failed to create a new doji context, DojiException.", e);
			return;
		} catch (SQLException e) {
			logger.warn("Failed to create a new doji context, SQLException.", e);
			return;
		} catch (Exception e) {
			logger.warn("Failed to close doji connection.", e);
		}

	}

	/**
	 * Creates a path for a file from the desired folder and the file name
	 * 
	 * @param existingPath - The Path to the Folder
	 * @param name         - The File's name
	 * @return Path of the File
	 */
	public String newPath(String existingPath, String name) {
		return existingPath + File.separator + name;
	}

	// SQL Statement to Get the Service from the Pm keys number
	private static final String FIND_SERVICE_FROM_PMKEYS = "SELECT SERVICE FROM MV_PMKEYS_DATA WHERE UNIQUE_EMPLOYEE_IDENTIFIER = ?";

	// SQL Statement to Get the Service from the Pm Keys number and Surname
	private static final String FIND_SERVICE_FROM_PMKEYS_AND_SURNAME = "SELECT SERVICE FROM MV_PMKEYS_DATA WHERE UNIQUE_EMPLOYEE_IDENTIFIER = ? AND lower(SURNAME) = lower(?)";

	/**
	 * Moves a file to the desired folder
	 * 
	 * @param file         - The file to be moved
	 * @param outputFolder - The path of the folder where the file is to be moved to
	 * @return Boolean - Tells whether the application can continue running or not.
	 *         If a folder is not found it will stop, false = stop
	 */
	private Boolean moveFile(File file, String outputFolder) {
		if (Files.notExists(Paths.get(outputFolder))) {
			logger.error("Destination Folder does not exist: " + outputFolder);
			return false;
		}
		
		if (outputFolder.equals(config.source)) {
			return true;
		}

		else if (!outputFolder.equals(config.failures)) {
			Boolean fileMoved = false;
			Integer count = 0;
			String newFileName = fileName;
			while (!fileMoved) {
				if (Files.notExists(Paths.get(newPath(outputFolder, newFileName)))) {
					try {

						Files.move(Paths.get(file.getPath()), Paths.get((newPath(outputFolder, newFileName))));
						fileMoved = true;

					} catch (IOException e) {
						logger.info(fileName
								+ " already exists in destination folder, or was not found. (Most likely file is still being copied to folder)");
						return true;
					}
				} else {
					// Already file with same name in folder
					logger.debug(
							"Folder Already Contains File: " + outputFolder + ", adding discriminator to file name");
					count++;
					newFileName = fileName.substring(0, fileName.lastIndexOf(".")) + "." + count
							+ fileName.substring(fileName.lastIndexOf("."));

				}
			}
		} else {
			sendToFailureFolder(file, newPath(outputFolder, fileName));
		}

		return true;
	}

	/**
	 * Sends a file to the failures folder. If a file already exists it adds
	 * .copy(*number) to the end of the files name.
	 *
	 * @param file        - what file is to be moved
	 * @param failurePath - the path to the failure folder with file in it
	 *
	 */
	public void sendToFailureFolder(File file, String failurePath) {
		Integer count = 1;
		String originalPath = failurePath;
		while (Files.exists(Paths.get(failurePath))) {
			if (originalPath.contains(".")) {
				failurePath = originalPath.substring(0, originalPath.lastIndexOf(".")) + "." + count
						+ originalPath.substring(originalPath.lastIndexOf("."));
			} else {
				failurePath = originalPath + "." + count;
			}

			count++;
		}
		try {
			Files.move(Paths.get(file.getPath()), Paths.get(failurePath));
			logger.info(fileName + " has failed, file has been moved to: " + failurePath);
		} catch (IOException e) {
			logger.error("Failed to move to the failures folder");
		}
	}

}
