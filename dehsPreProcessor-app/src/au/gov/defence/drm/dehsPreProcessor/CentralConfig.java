package au.gov.defence.drm.dehsPreProcessor;

import au.gov.defence.drm.centralconf.DrmCentralConfigProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsFactory;
//import io.reactivex.functions.Function;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Seems to extract all properties from the server central config (DrmCentralConfigProperties)
 * @author Rob Whittaker
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CentralConfig {
    private static final Logger logger =
            LoggerFactory.getLogger(CentralConfig.class);

    public final DojiConfig dojiConf;
    public final Properties props;
    
    public CentralConfig(@JsonProperty("doji") final DojiConfig doji) {
        this(doji, null);
    }

    private CentralConfig(final DojiConfig doji, final Properties props) {
        this.dojiConf = doji;
        this.props = props;
    }

    public CentralConfig(final CentralConfig centralConf) {
    	this(centralConf, null);
    }

    private CentralConfig(final CentralConfig centralConf, final Properties props) {
        this(centralConf == null ? null : centralConf.dojiConf, props);
    }

    /**
     *
     * @return returns a map of the centralConfig
     * @throws IOException - if the central config cannot be mapped
     */
    public static CentralConfig load() throws IOException {
        final JsonFactory factory = new JavaPropsFactory();

        final ObjectMapper mapper = new ObjectMapper(factory);

        logger.debug("Instantiate Central Config");
        final Properties props = new DrmCentralConfigProperties();

        // We read File into Properties, then write it back to a String, 
        // so we can parse it with the ObjectMapper. 
        // Very inefficient.. we do this cause "DrmCentralConfigProperties.class" does not 
        // expose the path to the file.
        final StringWriter writer = new StringWriter();
        props.store(writer, null);

        CentralConfig centralConfig =  mapper.readValue(writer.toString(), CentralConfig.class);
        
        return new CentralConfig(centralConfig, props);
    }

}
