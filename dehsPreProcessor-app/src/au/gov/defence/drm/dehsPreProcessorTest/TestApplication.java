package au.gov.defence.drm.dehsPreProcessorTest;


import static org.junit.Assert.assertTrue;

import java.io.File;

import java.nio.file.Paths;


import org.junit.BeforeClass;
import org.junit.Test;

import au.gov.defence.drm.dehsPreProcessor.AppConfig;
import au.gov.defence.drm.dehsPreProcessor.Monitor;

public class TestApplication {
    // Enter path to dehs.test
    static String testPath;

    static AppConfig config;

    static Boolean running = true;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        testPath =(new File("").getAbsolutePath()) + "\\src\\au\\gov\\defence\\drm\\dehsPreProcessorTest";
        System.out.println(testPath);
        config = AppConfig.load(Paths.get(testPath + "\\config.properties"));
        System.out.println(config.getPort());
        config.setTimebetweenfolderchecks(1);
    }


    @Test
    public void test2000values()  {
        TestUtils.cleanDirectorys(TestApplication.config);

        for (int i = 0; i < 2000; i++) {
            TestUtils.createFile(config.getSource()+ "/UMR-8020146-Neal-3001-EY2010-20100912." + i + ".PDF");
        }


        while(new File(config.getSource()).listFiles().length > 0 && running) {
            try {
                new Monitor(config, true);
            } catch (InterruptedException e) {
                running = false;
            }
        }

        assertTrue(TestUtils.numberFilesInFolder(config.getArmy_nonserving()) == 2000);
    }





}
