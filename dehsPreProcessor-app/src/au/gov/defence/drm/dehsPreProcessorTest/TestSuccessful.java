package au.gov.defence.drm.dehsPreProcessorTest;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import org.junit.BeforeClass;
import org.junit.Test;

import au.gov.defence.drm.dehsPreProcessor.AppConfig;

public class TestSuccessful {
	// Enter path to dehs.test
	static String testPath;

	static AppConfig config;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		testPath = (new File("").getAbsolutePath()) + "\\src\\au\\gov\\defence\\drm\\dehsPreProcessorTest";
		System.out.println(testPath);
		config = AppConfig.load(Paths.get(testPath + "\\config.properties"));
		System.out.println(config.getPort());
		config.setTimebetweenfolderchecks(1);
	}

	@Test
	public void testArmyNonServing() {
		TestUtils.cleanDirectorys(TestSuccessful.config);
		TestUtils.createFile(config.getSource() + "/UMR-8020146-Neal-3001-EY2010-20100912.PDF");
		TestUtils.runMonitorForTesting(config);
		assertTrue(TestUtils.numberFilesInFolder(config.getArmy_nonserving()) == 1);
	}

	@Test
	public void testArmyServing() {
		TestUtils.cleanDirectorys(TestSuccessful.config);
		TestUtils.createFile(config.getSource() + "/UMR-8020146-Neal-3231-EY2010-20100912.PDF");
		TestUtils.runMonitorForTesting(config);
		assertTrue(TestUtils.numberFilesInFolder(config.getArmy_serving()) == 1);
	}

	@Test
	public void testNavyNonServing() {
		TestUtils.cleanDirectorys(TestSuccessful.config);
		TestUtils.createFile(config.getSource() + "/UMR-8021008-Fraser-3001-EY2010-20100912.PDF");
		TestUtils.runMonitorForTesting(config);
		assertTrue(TestUtils.numberFilesInFolder(config.getNavy_nonserving()) == 1);
	}

	@Test
	public void testNavyServing() {
		TestUtils.cleanDirectorys(TestSuccessful.config);
		TestUtils.createFile(config.getSource() + "/UMR-8021008-Fraser-3121-EY2010-20100912.PDF");
		TestUtils.runMonitorForTesting(config);
		assertTrue(TestUtils.numberFilesInFolder(config.getNavy_serving()) == 1);
	}

	@Test
	public void testRaafNonServing() {
		TestUtils.cleanDirectorys(TestSuccessful.config);
		TestUtils.createFile(config.getSource() + "/UMR-8021375-Warren-3001-EY2010-20100912.PDF");
		TestUtils.runMonitorForTesting(config);
		assertTrue(TestUtils.numberFilesInFolder(config.getRaaf_nonserving()) == 1);
	}

	@Test
	public void testRaafServing() {
		TestUtils.cleanDirectorys(TestSuccessful.config);
		TestUtils.createFile(config.getSource() + "/UMR-8021375-Warren-3013-EY2010-20100912.PDF");
		TestUtils.runMonitorForTesting(config);
		assertTrue(TestUtils.numberFilesInFolder(config.getRaaf_serving()) == 1);
	}

	@Test
	public void testEmptyList() throws IOException {
		config.setForms_nonserving(null);
		TestUtils.cleanDirectorys(TestSuccessful.config);
		TestUtils.createFile(config.getSource() + "/UMR-8020146-Neal-3001-EY2010-20100912.PDF");
		TestUtils.runMonitorForTesting(config);
		assertTrue(TestUtils.numberFilesInFolder(config.getArmy_serving()) == 1);

		config = AppConfig.load(Paths.get(testPath + "\\config.properties"));

	}

}
