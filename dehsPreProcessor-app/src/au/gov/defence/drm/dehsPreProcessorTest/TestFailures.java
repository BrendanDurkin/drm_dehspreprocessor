package au.gov.defence.drm.dehsPreProcessorTest;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.Paths;

import org.junit.BeforeClass;
import org.junit.Test;

import au.gov.defence.drm.dehsPreProcessor.AppConfig;

public class TestFailures {
    // Enter path to dehs.test
    // You must have your email setup
    static String testPath;

    static AppConfig config;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        testPath =(new File("").getAbsolutePath()) + "\\src\\au\\gov\\defence\\drm\\dehsPreProcessorTest";
        System.out.println(testPath);
        config = AppConfig.load(Paths.get(testPath + "\\config.properties"));
        System.out.println(config.getPort());
        config.setTimebetweenfolderchecks(1);
    }

    @Test
    public void duplicateFileInDestinationFolder() {
        TestUtils.cleanDirectorys(TestFailures.config);
        TestUtils.createFile(config.getSource()+ "/UMR-8020146-Neal-3001-EY2010-20100912.PDF");
        TestUtils.runMonitorForTesting(config);
        TestUtils.createFile(config.getSource()+ "/UMR-8020146-Neal-3001-EY2010-20100912.PDF");
        TestUtils.runMonitorForTesting(config);
        assertTrue(TestUtils.numberFilesInFolder(config.getArmy_nonserving()) == 2);
    }

    @Test
    public void nonMatchingPmKeysAndSurname() {
        TestUtils.cleanDirectorys(TestFailures.config);
        TestUtils.createFile(config.getSource()+ "/UMR-8020146-WrongName-3001-EY2010-20100912.PDF");
        TestUtils.runMonitorForTesting(config);
        assertTrue(TestUtils.numberFilesInFolder(config.getFailures()) == 1);
    }

    @Test
    public void invalidService() {
        TestUtils.cleanDirectorys(TestFailures.config);
        TestUtils.createFile(config.getSource()+ "/UMR-8513994-Fabre-3001-EY2010-20100912.PDF");
        TestUtils.runMonitorForTesting(config);
        assertTrue(TestUtils.numberFilesInFolder(config.getFailures()) == 1);
    }

    @Test
    public void invalidPmKeysOrSurname() {
        TestUtils.cleanDirectorys(TestFailures.config);
        TestUtils.createFile(config.getSource()+ "/UMR-213123123123123123-123123123123-3001-EY2010-20100912.PDF");
        TestUtils.runMonitorForTesting(config);
        assertTrue(TestUtils.numberFilesInFolder(config.getFailures()) == 1);
    }

    @Test
    public void invalidFileName() {
        TestUtils.cleanDirectorys(TestFailures.config);
        TestUtils.createFile(config.getSource()+ "/123123123123123123");
        TestUtils.runMonitorForTesting(config);
        assertTrue(TestUtils.numberFilesInFolder(config.getFailures()) == 1);
    }

    @Test
    public void duplicateFileInFailureFolder() {
        TestUtils.cleanDirectorys(TestFailures.config);
        TestUtils.createFile(config.getSource()+ "/1");
        TestUtils.runMonitorForTesting(config);
        TestUtils.createFile(config.getSource()+ "/1");
        TestUtils.runMonitorForTesting(config);
        TestUtils.createFile(config.getSource()+ "/1");
        TestUtils.runMonitorForTesting(config);

        assertTrue(TestUtils.numberFilesInFolder(config.getFailures()) == 3);
    }





}
