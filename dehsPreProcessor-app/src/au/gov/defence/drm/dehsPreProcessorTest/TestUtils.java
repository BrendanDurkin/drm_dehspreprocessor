package au.gov.defence.drm.dehsPreProcessorTest;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import au.gov.defence.drm.dehsPreProcessor.AppConfig;
import au.gov.defence.drm.dehsPreProcessor.Monitor;

public class TestUtils {

    public static void cleanDirectorys(AppConfig config)  {

        try {
            FileUtils.cleanDirectory(new File(config.getSource()));
            FileUtils.cleanDirectory(new File(config.getFailures()));
            FileUtils.cleanDirectory(new File(config.getArmy_nonserving()));
            FileUtils.cleanDirectory(new File(config.getArmy_serving()));
            FileUtils.cleanDirectory(new File(config.getNavy_nonserving()));
            FileUtils.cleanDirectory(new File(config.getNavy_serving()));
            FileUtils.cleanDirectory(new File(config.getRaaf_nonserving()));
            FileUtils.cleanDirectory(new File(config.getRaaf_serving()));
        } catch (IOException e) {
            // TODO Auto-generated catch block
        }
    }

    public static void createFile(String filename) {
        File test = new File(filename);
        try {
            test.createNewFile();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void runMonitorForTesting(AppConfig config) {
        try {
            new Monitor(config, true);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static Integer numberFilesInFolder(String folder) {
       return (new File(folder).listFiles().length);
    }

}
