package au.gov.defence.drm.dehsPreProcessorTest;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsFactory;

import au.gov.defence.drm.centralconf.DrmCentralConfigProperties;
import au.gov.defence.drm.dehsPreProcessor.AppConfig;
import au.gov.defence.drm.dehsPreProcessor.CentralConfig;

public class TestProperties {
	//@Test
	public void testLoad() throws URISyntaxException, IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Path path = getAppConfigPath();
		setCentralConfigPath();

		AppConfig app = AppConfig.load(path);
		
		System.out.println(app.getNavy_serving());
		System.out.println(app.getNavy_nonserving());
		
	}
	
	//@Test
	public void testMapperDirectRead() throws IOException, URISyntaxException {
		
		Path path = getAppConfigPath();
		String text = readFile(path);

		final ObjectMapper mapper = new ObjectMapper(new JavaPropsFactory());
		AppConfig app = mapper.readValue(text, AppConfig.class);
        		
		System.out.println(app.getNavy_serving());
		System.out.println(app.getNavy_nonserving());
        

	}
	
	private String readFile(Path path) throws IOException {
		return Files.readAllLines(path).stream().collect(Collectors.joining(System.lineSeparator()));
	}

	private Path getAppConfigPath() throws URISyntaxException {
		URL url = TestProperties.class.getClassLoader().getResource("test_config.properties");
		Path path = Paths.get(url.toURI());
		assertTrue(Files.exists(path));
		return path;
	}
	
	private Path setCentralConfigPath() throws NoSuchFieldException, SecurityException, URISyntaxException, IllegalArgumentException, IllegalAccessException {
		URL url = TestProperties.class.getClassLoader().getResource("test_central_config.properties");
		Path path = Paths.get(url.toURI());
		assertTrue(Files.exists(path));
		Field x;
		x = DrmCentralConfigProperties.class.getDeclaredField("defaultCentralConfigFilePathWindows");
		x.setAccessible(true);
		x.set(null, path.toString());
		return path;
	}
	
	@Test 
	public void readDirectly() throws URISyntaxException, IOException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
	
	   // Path centralPath = setCentralConfigPath();
		
	    Path[] paths = { setCentralConfigPath(), getAppConfigPath()};
	    
	    AppConfig appConfig = new AppConfig();	            
	    final ObjectMapper mapper = new ObjectMapper(new JavaPropsFactory());
	    for (Path path: paths) {
		    appConfig = mapper.readerForUpdating(appConfig).readValue(Files.newInputStream(path));	    		    	
	    }
	    
	    assertNotNull(appConfig.getDoji());
	    assertNotNull(appConfig.getDoji().server);
	}
}
