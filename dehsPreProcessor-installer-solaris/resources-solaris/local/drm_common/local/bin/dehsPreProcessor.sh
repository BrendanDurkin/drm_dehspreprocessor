#!/bin/bash
echo "-------------------------------------------------------"
echo "DEHS Pre-Processor - Service Control Script"
echo ""

APP="/apps/drm_dehsPreProcessor/local/bin/dehsPreProcessor_service.sh"

ARGS="$@"
_USER=`/usr/ucb/whoami`
if [ "${_USER}" = "objweb" ]; then
   ${APP} ${ARGS}
else    
   echo "Please validate your authority by entering your unix password"
   ARGS="$@"
   sudo -u objweb ${APP} ${ARGS}
   if [ $? -ne 0 ]; then
      echo "You need to be a member of objweb to run this script"
   fi
fi   
