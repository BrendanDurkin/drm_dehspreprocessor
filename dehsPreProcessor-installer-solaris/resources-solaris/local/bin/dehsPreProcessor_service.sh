#!/usr/bin/bash

#--------------------------------------------------------
usage() {
cat <<EOD

Usage: $0 <flags> [INSTANCE_1 [INSTANCE_2] ...[INSTANCE_n]]] [COMMAND]
    
    INSTANCE    : The name(s) of the instances to effect.
                  if not provided the DEFAULT_INSTANCE_LIST in 
                  /apps/drm_common/local/etc/install.defaults 
                  is used
 
    COMMAND: 
        start   : Starts the application and is the default value
        stop    : Stops the application
                
   FLAGS:
        -q          : Quiet mode. Do not tail log file when starting. 
        -s          : Stop (Alternative to usting keyword stop)
        -h          : help
        -d <port>   : Enable java remote debug on port (no wait)
        -D <port>   : Enable java remote debug on port (with wait)
EOD
exit 1
}

#--------------------------------------------------------
parseArgs() {
    _DEBUG=""
    _PATH=""
    PRODUCT="drm_dehsPreProcessor"
    PRODUCT_INSTANCE=""
    _START=1
    _QUIET=0
    while getopts "hqsd:D:" opt ; do
        case $opt in
            q) _QUIET=1;;
            s) _START=0;;
            d) _DEBUG="-d $OPTARG";;
            D) _DEBUG="-D $OPTARG";;
            h) usage;;
           \?) usage;;
        esac
    done
    shift $(($OPTIND - 1))
    if [ $# -gt 0 ]; then 
        for INSTANCE in $@; do
        if [ "${INSTANCE}" = "start" ]; then 
            _START=1
        elif [ "${INSTANCE}" = "stop" ]; then
            _START=0
        else
            INSTANCE_LIST="${INSTANCE_LIST} ${INSTANCE}"
            PRODUCT_INSTANCE="${INSTANCE}"
            INSTANCE_PATH="/apps/${PRODUCT}/admin/${INSTANCE}"
            if [ ! -d  ${INSTANCE_PATH}/log ]; then
                if [ ${_QUIET} -eq 0 ]; then
                    echo "Error: instance ${INSTANCE} not found" >&2
                    usage
                fi
                exit 1
            fi
        fi
        done
        INSTANCE_LIST=`echo ${INSTANCE_LIST}`
    else    
        INSTANCE_LIST=""
    fi
   
    if [ "x${INSTANCE_LIST}" = "x" ]; then
        . /apps/drm_common/local/etc/install.defaults
        INSTANCE_LIST="${DEFAULT_INSTANCE_LIST}"      
    fi
    if [ "x${INSTANCE_LIST}" = "x" ]; then
        if [ ${_QUIET} -eq 0 ]; then
            echo "Invalid Params" >&2
            usage  >&2
        fi
        exit 1
    fi   
}

#--------------------------------------------------------
start() {
    CHECK=`${WRAPPER} ${PRODUCT} ${INSTANCE} check`
    if [ "${CHECK}" = "" ]; then
        if [ -f ${INSTANCE_PATH}/log/${PRODUCT}.log ]; then
            mv ${INSTANCE_PATH}/log/${PRODUCT}.log ${INSTANCE_PATH}/log/${PRODUCT}_`date '+%y%m%d:%H:%M:%S'`.log
        fi  
        LOG_FILE="${INSTANCE_PATH}/log/${PRODUCT}_wrapper.log"
        if [ -f ${LOG_FILE} ]; then
            mv ${LOG_FILE} ${INSTANCE_PATH}/log/${PRODUCT}_wrapper_`date '+%y%m%d:%H:%M:%S'`.log
        fi   
          
        if [ "${_PATH}" = "" ]; then
            _PATH="${_DEFAULT_PROPERTIES_PATH}"
        fi
# Ivan: I have no idea how these scripts are supposed to work.
#       What I mean is I can see what they do, it just makes no sense
#       to me that they were written this way.
#       The arguments added after start appear to be added to the wrapper
#       conf file every time the app is run. How did this ever work?
#       Not sure why this doesn't happen in say, BOJI.
        #${WRAPPER} ${_DEBUG} ${PRODUCT} ${INSTANCE} start -c ${_PATH}
        ${WRAPPER} ${_DEBUG} ${PRODUCT} ${INSTANCE} start
        if [ ${NUMBER_STARTED} -gt 0 ]; then
            NUMBER_STARTED=2
        else  
            VERIFY_LOG="${LOG_FILE}"
            NUMBER_STARTED=1
        fi 
    elif [ "${CHECK}" = "running" ]; then
        if [ ${_QUIET} -eq 0 ]; then
            echo "Unable to start. Process already running" >&2
        fi
    else
        if [ ${_QUIET} -eq 0 ]; then
            echo "Unable to start. Config error" >&2     
            echo ${CHECK} >&2
        fi
        EXIT_CODE=1
    fi   
}

#--------------------------------------------------------
stop() {
    echo "Attempting to stop ${PRODUCT}. This may take up to 2 minutes. Please be patient."
    ${WRAPPER} ${PRODUCT} ${INSTANCE} stop
}

#--------------------------------------------------------
verify() {
    if [ ${_QUIET} -eq 0 ]; then
        X=0
        while [ ! -f ${VERIFY_LOG} ]; do 
            echo "waiting for log file to be created"
            sleep 1
            X=`expr ${X} + 1`
            if [ ${X} -gt 50 ]; then
                echo "Warning: no log file has been created !"
                echo "         Please investigate why the service has not started."
                exit 1
            fi
        done
        echo "Log file found."  
        echo ""
        echo ""
        echo "${PRODUCT} has started."
        echo "  Please watch the log for error messages for the first 2 minutes of"
        echo "  operation."
        echo "  ...continuously tailing log.                   (press ctl-c to exit)."
        echo "-----------------------------------------------------------------------"
        tail -f ${VERIFY_LOG}
    fi
}

#--------------------------------------------------------
checkCommonLink() {
    LINK="${INSTANCE_PATH}/product/common"
    if [ ! -L "${LINK}" ]; then
        echo "WARNING: The symbolic link ${LINK} is broken"
        echo "         Attempting to automatically repair the link to the"
        echo "         latest version of drm_common"
        _COMMON_VERISON="/apps/drm_common/local/bin/version.sh drm_common"
        if [ -L ${LINK} ]; then
            rm ${LINK}
            if [ -L ${LINK} ]; then
                echo "ERROR: Insufficient privileges to remove the broken link"
                exit 1
            fi 
        fi 
        ln -s /apps/drm_common/product/${_COMMON_VERISON} ${LINK}
        chmod 775 ${LINK} 2>&1 1> /dev/null
    fi 
    if [ ! -L ${LINK} ]; then
        echo "ERROR: Unable to fix the broken link"
        exit 1
    fi 
}

#--------------------------------------------------------
main() {
    umask 002
    parseArgs $@
    EXIT_CODE=0
    LOG_FILE=""
    NUMBER_STARTED=0
    _RELATIVE_PATH_TO_PROPERTIES="etc/preprocessor_config.yaml"
    _DEFAULT_PROPERTIES_PATH=""
    for INSTANCE in ${INSTANCE_LIST}; do
        INSTANCE_PATH="/apps/${PRODUCT}/admin/${INSTANCE}"
        if [ ! -d  ${INSTANCE_PATH}/log ]; then
            if [ ${_QUIET} -eq 0 ]; then
                echo "Error: instance ${INSTANCE} not found" >&2
                usage >&2
            fi
            exit 1
        fi       
        LOG_FILE="${INSTANCE_PATH}/log/${PRODUCT}_wrapper.log"
        WRAPPER="${INSTANCE_PATH}/product/bin/wrapper.sh"
        _DEFAULT_PROPERTIES_PATH="${INSTANCE_PATH}/${_RELATIVE_PATH_TO_PROPERTIES}"
        checkCommonLink
    
        if [ ${_START} -eq 1 ]; then
            if [ ${_QUIET} -eq 0 ]; then
                echo "Starting ${PRODUCT} ${INSTANCE} instance"
            fi   
            start
        else
            if [ ${_QUIET} -eq 0 ]; then
                echo "Stopping ${PRODUCT} ${INSTANCE} instance"
            fi   
            stop
        fi      
  done
  if [ ${NUMBER_STARTED} -eq 1 ]; then
     verify
  fi
}

#--------------------------------------------------------
#--------------------------------------------------------
main $@
exit ${EXIT_CODE}   
