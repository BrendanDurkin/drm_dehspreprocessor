# the following are defined by the calling script
#    _PRODUCT_PATH="/apps/${PRODUCT}/product/${VERSION}"
#    _COMMON_PATH="/apps/drm_common/product/${COMMONLIB}"
#    PRODUCT=?
#    _INSTALL=?
#    _YES=1
#    _NO=0
#    _SKIP_SQL=?
#    INSTANCE_LIST="?..."


     
#------------------------------------------------------------------------------
# customInit
#------------------------------------------------------------------------------
customInit() {
    commonInit
}

#------------------------------------------------------------------------------
# customInstanceManager
#------------------------------------------------------------------------------
customInstanceManager() {
    if [ "${INSTANCE_LIST}" != "" ]; then
        for INSTANCE in ${INSTANCE_LIST}; do
            _INSTANCE_PATH="/apps/${PRODUCT}/admin/${INSTANCE}"
            if [ ${_INSTALL} -eq ${_YES} ]; then 
                customCreateInstance
            else
                customDeleteInstance   
            fi
        done
    fi
}

#------------------------------------------------------------------------------
# customDeleteInstance
#------------------------------------------------------------------------------
customDeleteInstance() {
    #_INSTANCE_PATH="/apps/${PRODUCT}/admin/${INSTANCE}"
    if [ -d ${_INSTANCE_PATH} ]; then
        if [ ${_SKIP_SQL} -eq ${_NO} ]; then 
            commonUninstallSql
            commonShowErrors
        fi 
        rm -Rf ${_INSTANCE_PATH}/data/import/* 2>&1 1>/dev/null
        rm -Rf ${_INSTANCE_PATH}/data/imported/* 2>&1 1>/dev/null
        rm -Rf ${_INSTANCE_PATH}/data/exported/* 2>&1 1>/dev/null
        rm -Rf ${_INSTANCE_PATH}/data/failed/* 2>&1 1>/dev/null
      
        commonDeleteInstanceLinks
        commonDeleteInstanceFolder
    fi
}

#------------------------------------------------------------------------------
# customCreateInstance
#------------------------------------------------------------------------------
customCreateInstance() {
    commonCreateInstanceFolder   
    commonCreateInstanceLinks
    customBuildWrapper  
    chmod 664 ${_INSTANCE_PATH}/etc/*.xml
    chmod 664 ${_INSTANCE_PATH}/etc/*.yaml
    chmod 664 ${_INSTANCE_PATH}/etc/*.properties
}

#------------------------------------------------------------------------------
# customPreremoveProduct
#------------------------------------------------------------------------------
customPreremoveProduct() {
    commonPreremoveProduct
}

#------------------------------------------------------------------------------
# customPostInstallProduct
#------------------------------------------------------------------------------
customPostInstallProduct() {
    commonPostInstallProduct
}
      
#------------------------------------------------------------------------------
# customLinkInstances
#------------------------------------------------------------------------------
customReLinkInstances() {
    commonReLinkInstances
}

#------------------------------------------------------------------------------
# customBuildWrapper
# removes dependancy on libraries in drm_common
#------------------------------------------------------------------------------
customBuildWrapper() {
    ${_PRODUCT_PATH}/bin/build_wrapper_conf.sh ${PRODUCT} ${INSTANCE}
}
