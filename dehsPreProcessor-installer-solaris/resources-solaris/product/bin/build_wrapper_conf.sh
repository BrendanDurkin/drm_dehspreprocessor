#!/bin/ksh 

#------------------------------------------------------------------------------     
# Set $1 as a library path
#------------------------------------------------------------------------------     
setLib() {
    if [ -d $1 ]; then
        echo "wrapper.java.library.path.${_LIB_COUNTER}=$1" >> ${_WRAPPER_CONF}
        _LIB_COUNTER=`expr ${_LIB_COUNTER} + 1`
        LD_LIBRARY="$LD_LIBRARY:$1"
    else
        echo "Warning : ld path '$1' not found." >&2
    fi
}

#------------------------------------------------------------------------------     
# Set $1 as a class path
#------------------------------------------------------------------------------     
setClass() {
    if [ -f $1 ]; then
        echo "wrapper.java.classpath.${_CLASS_COUNTER}=$1" >> ${_WRAPPER_CONF}
        _CLASS_COUNTER=`expr ${_CLASS_COUNTER} + 1`
    else
        echo "Warning : jar class '$1' not found." >&2
    fi   
}

#------------------------------------------------------------------------------     
# discover all jar files in $1 and set then as class paths
#------------------------------------------------------------------------------     
setClasses() {
    if [ -d $1 ]; then
        for _PATH in `find $1 -name *.jar`; do
            setClass "${_PATH}"
        done
    fi   
}

#------------------------------------------------------------------------------     
# Set the service title, name, desctiption properties
#------------------------------------------------------------------------------     
setTitles() {
    echo "wrapper.console.title=${PRODUCT} Service Wrapper Application" >> ${_WRAPPER_CONF}
    echo "wrapper.ntservice.name=${PRODUCT}" >> ${_WRAPPER_CONF}
    echo "wrapper.ntservice.displayname=${PRODUCT} Application" >> ${_WRAPPER_CONF}
    echo "wrapper.ntservice.description=${PRODUCT} Application Description" >> ${_WRAPPER_CONF}
}

#------------------------------------------------------------------------------     
# Set the log output 
#------------------------------------------------------------------------------     
setLog() {
    echo "wrapper.logfile=${_ROOT}/log/${PRODUCT}_wrapper.log" >> ${_WRAPPER_CONF}
}

#------------------------------------------------------------------------------     
setParams() {
    if [ -f ${_ROOT}/etc/main_class.txt ]; then
        MAINCLASS=`cat ${_ROOT}/etc/main_class.txt`
        echo "wrapper.app.parameter.1=${MAINCLASS}" >> ${_WRAPPER_CONF}
    else 
        echo "wrapper.app.parameter.1=-c" >> ${_WRAPPER_CONF}
        echo "wrapper.app.parameter.2=${_ROOT}/etc/${PRODUCT}_config.yaml" >> ${_WRAPPER_CONF}
    fi
}

#------------------------------------------------------------------------------     
parseArgs() {
    if [ $# -gt 3 ]; then
        echo "Error: too many parameters" >&2
        `usage` >&2
        exit 1
   elif [ $# -gt 1 ]; then
        PRODUCT="$1"
        INSTANCE="$2"
        _ROOT="/apps/${PRODUCT}/admin/${INSTANCE}"      
        _TEMPLATE_CONF="$3"
        if [ "${_TEMPLATE_CONF}" = "" ]; then
            _TEMPLATE_CONF="${_ROOT}/etc/wrapper.template"
        fi
        _WRAPPER_CONF="${_ROOT}/etc/${PRODUCT}_wrapper.conf"
        _CLASS_COUNTER=1
        _LIB_COUNTER=1
    else 
        echo "Error: insuficient parameters" >&2
        `usage` >&2 
        exit 1
    fi
}

#------------------------------------------------------------------------------     
usage () {
    echo "" 
    echo "Usage"
    echo "      $0 <product> <instance>"
    echo ""
}

#------------------------------------------------------------------------------     
# main
#------------------------------------------------------------------------------     
# build the classpath text...
parseArgs $@
if [ -f "${_TEMPLATE_CONF}" ]; then
    if [ -f  ${_WRAPPER_CONF} ]; then
        echo "Warning: Skipping auto create '${_WRAPPER_CONF}'. File already exists"
    else
        touch  ${_WRAPPER_CONF}
        setClasses "${_ROOT}/product/lib"
        LD_LIBRARY="."
        setLib "${_ROOT}/product/lib/"
        echo "set.LD_LIBRARY_PATH=$LD_LIBRARY" >> ${_WRAPPER_CONF}
        chown objweb:objweb ${_WRAPPER_CONF}
        chmod g+rw ${_WRAPPER_CONF}
        setTitles
        setLog
        setParams
        grep -v '^wrapper.java.classpath.' ${_TEMPLATE_CONF} | grep -v '^wrapper.java.library.path.' >> ${_WRAPPER_CONF}
    fi
fi
