#
#  Note:  param 1 : product
#         param 2 : instance
#         param 3 : mode (i.e. {start | stop | restart | console } )
#         param 4 : user (generally not used)
#         
#
#
#! /bin/sh

#
# Copyright (c) 1999, 2006 Tanuki Software Inc.
#
# Java Service Wrapper sh script.  Suitable for starting and stopping
#  wrapped Java applications on UNIX platforms.
#

#-----------------------------------------------------------------------------
# These settings can be modified to fit the needs of your application

#----------------------------------------------------------------------
usage() {
cat <<EOD

Usage: $0 [-d|-D <PORT>] [-u <USER>] <PRODUCT> <INSTANCE> <MODE> [<java app flags>]

  Params:
      PRODUCT  The name of the product. 
               Must reside in /apps/<PRODUCT>
      INSTANCE The name of the product instance. 
               Must reside in /apps/<PRODUCT>/admin/<INSTANCE>
      MODE     start | stop | restart | console | check
      USER     Run as user...

  Flags: 
      -d PORT  Run in debug mode listening on port <PORT>
      -D PORT  Run in debug mode listening on port <PORT>, 
               suspend service till remote debugger attaches
      -u USER  Run as user...        

EOD
   doExit 1;
}

#--------------------------------------------------------
log() {
   #echo "$$ - $@"  >> /tmp/wrapper.log
   _DUMMY=1
}

#--------------------------------------------------------
doExit() {
   log "done."
   exit $1
}

#--------------------------------------------------------
parseArgs() {
   # Get the fully qualified path to the script
   case $0 in
       /*)
           SCRIPT="$0"
           ;;
       *)
           PWD=`pwd`
           SCRIPT="$PWD/$0"
           ;;
   esac
   
   # We are using -D options to set system variables (not to put into debug mode) so need to 
   # remove D option here so we don't get and use debug wrapper conf file.
   _DEBUG_FLAG=0
   PORT=""
   USER=""
   _USAGE=0
   #while getopts "hd:D:u:" opt ; do
   while getopts "hd:u:" opt ; do
      case $opt in
            u) USER="$OPTARG";;
            d) _DEBUG_FLAG=1;
               PORT="$OPTARG";;
            #D) _DEBUG_FLAG=2;
            #   PORT="$OPTARG";;
            h) _USAGE=1;;
           \?) _USAGE=1;;
     esac
   done
   shift $(($OPTIND - 1))

   PRODUCT="$1"
   INSTANCE="$2"
   MODE="$3"
   shift 3
   ARGS="$@"
   
   log "-------------------------------------------------------"
   log "start: PRODUCT=$PRODUCT, INSTANCE=$INSTANCE, MODE=$MODE"
   if [ $_USAGE -eq 1 ]; then
      usage
   fi
}
   
#----------------------------------------------------------
parseArgs $@

export ORACLE_HOME=`/apps/${PRODUCT}/admin/${INSTANCE}/product/common/bin/getOracleHome.sh -a ${INSTANCE}`
export LD_LIBRARY_PATH="${ORACLE_HOME}/lib"


# Application
APP_NAME="${PRODUCT}_${INSTANCE}"
APP_LONG_NAME="${PRODUCT} ${INSTANCE} Application"

# Wrapper
WRAPPER_CMD="/apps/${PRODUCT}/admin/${INSTANCE}/wrapper"
WRAPPER_CONF="/apps/${PRODUCT}/admin/${INSTANCE}/etc/${PRODUCT}_wrapper.conf"
DEBUG_CONF="/apps/${PRODUCT}/admin/${INSTANCE}/etc/${PRODUCT}_wrapper_debug.conf"

# Priority at which to run the wrapper.  See "man nice" for valid priorities.
#  nice is only used if a priority is specified.
PRIORITY=

# Location of the pid file.
PIDDIR="/apps/${PRODUCT}/admin/${INSTANCE}/log"

# If uncommented, causes the Wrapper to be shutdown using an anchor file.
#  When launched with the 'start' command, it will also ignore all INT and
#  TERM signals.
#IGNORE_SIGNALS=true

# Wrapper will start the JVM asynchronously. Your application may have some
#  initialization tasks and it may be desirable to wait a few seconds
#  before returning.  For example, to delay the invocation of following
#  startup scripts.  Setting WAIT_AFTER_STARTUP to a positive number will
#  cause the start command to delay for the indicated period of time 
#  (in seconds).
# 
WAIT_AFTER_STARTUP=0

# If set, the status, start_msg and stop_msg commands will print out detailed
#   state information on the Wrapper and Java processes.
#DETAIL_STATUS=true

# If specified, the Wrapper will be run as the specified user.
# IMPORTANT - Make sure that the user has the required privileges to write
#  the PID file and wrapper.log files.  Failure to be able to write the log
#  file will cause the Wrapper to exit without any way to write out an error
#  message.
# NOTE - This will set the user which is used to run the Wrapper as well as
#  the JVM and is not useful in situations where a privileged resource or
#  port needs to be allocated prior to the user being changed.
RUN_AS_USER=${USER}

# The following two lines are used by the chkconfig command. Change as is
#  appropriate for your application.  They should remain commented.
# chkconfig: 2345 20 80
# description: Test Wrapper Sample Application

# Initialization block for the install_initd and remove_initd scripts used by
#  SUSE linux distributions.
### BEGIN INIT INFO
# Provides: @app.name@
# Required-Start: $local_fs $network $syslog
# Should-Start: 
# Required-Stop:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: @app.long.name@
# Description: @app.description@
### END INIT INFO

# Do not modify anything beyond this point
#-----------------------------------------------------------------------------

# Resolve the true real path without any sym links.
CHANGED=true
while [ "X$CHANGED" != "X" ]
do
    # Change spaces to ":" so the tokens can be parsed.
    SAFESCRIPT=`echo $SCRIPT | sed -e 's; ;:;g'`
    # Get the real path to this script, resolving any symbolic links
    TOKENS=`echo $SAFESCRIPT | sed -e 's;/; ;g'`
    REALPATH=
    for C in $TOKENS; do
        # Change any ":" in the token back to a space.
        C=`echo $C | sed -e 's;:; ;g'`
        REALPATH="$REALPATH/$C"
        # If REALPATH is a sym link, resolve it.  Loop for nested links.
        while [ -h "$REALPATH" ] ; do
            LS="`ls -ld "$REALPATH"`"
            LINK="`expr "$LS" : '.*-> \(.*\)$'`"
            if expr "$LINK" : '/.*' > /dev/null; then
                # LINK is absolute.
                REALPATH="$LINK"
            else
                # LINK is relative.
                REALPATH="`dirname "$REALPATH"`""/$LINK"
            fi
        done
    done

    if [ "$REALPATH" = "$SCRIPT" ]
    then
        CHANGED=""
    else
        SCRIPT="$REALPATH"
    fi
done

# Change the current directory to the location of the script
cd "`dirname "$REALPATH"`"
REALDIR=`pwd`

# If the PIDDIR is relative, set its value relative to the full REALPATH to avoid problems if
#  the working directory is later changed.
FIRST_CHAR=`echo $PIDDIR | cut -c1,1`
if [ "$FIRST_CHAR" != "/" ]
then
    PIDDIR=$REALDIR/$PIDDIR
fi
# Same test for WRAPPER_CMD
FIRST_CHAR=`echo $WRAPPER_CMD | cut -c1,1`
if [ "$FIRST_CHAR" != "/" ]
then
    WRAPPER_CMD=$REALDIR/$WRAPPER_CMD
fi
# Same test for WRAPPER_CONF
FIRST_CHAR=`echo $WRAPPER_CONF | cut -c1,1`
if [ "$FIRST_CHAR" != "/" ]
then
    WRAPPER_CONF=$REALDIR/$WRAPPER_CONF
fi

# Same test for WRAPPER_CONF
FIRST_CHAR=`echo $DEBUG_CONF | cut -c1,1`
if [ "$FIRST_CHAR" != "/" ]
then
    DEBUG_CONF=$REALDIR/$DEBUG_CONF
fi

# Process ID
ANCHORFILE="$PIDDIR/$APP_NAME.anchor"
STATUSFILE="$PIDDIR/$APP_NAME.status"
JAVASTATUSFILE="$PIDDIR/$APP_NAME.java.status"
PIDFILE="$PIDDIR/$APP_NAME.pid"
LOCKDIR="/var/lock/subsys"
LOCKFILE="$LOCKDIR/$APP_NAME"
pid=""

# Resolve the location of the 'ps' command
PSEXE="/usr/bin/ps"
if [ ! -x "$PSEXE" ]
then
    PSEXE="/bin/ps"
    if [ ! -x "$PSEXE" ]; then
        echo "Unable to locate 'ps'."
        echo "Please report this message along with the location of the command on your system."
        doExit 1
    fi
fi

# Resolve the os
DIST_OS=`uname -s | tr [:upper:] [:lower:] | tr -d [:blank:]`
case "$DIST_OS" in
    'sunos')
        DIST_OS="solaris"
        ;;
    'hp-ux' | 'hp-ux64')
        # HP-UX needs the XPG4 version of ps (for -o args)
        DIST_OS="hpux"
        UNIX95=""
        export UNIX95   
        ;;
    'darwin')
        DIST_OS="macosx"
        ;;
    'unix_sv')
        DIST_OS="unixware"
        ;;
esac

# Resolve the architecture
if [ "$DIST_OS" = "macosx" ]; then
    DIST_ARCH="universal"
else
    DIST_ARCH=
    DIST_ARCH=`uname -p 2>/dev/null | tr [:upper:] [:lower:] | tr -d [:blank:]`
    if [ "X$DIST_ARCH" = "X" ]
    then
        DIST_ARCH="unknown"
    fi
    if [ "$DIST_ARCH" = "unknown" ]
    then
        DIST_ARCH=`uname -m 2>/dev/null | tr [:upper:] [:lower:] | tr -d [:blank:]`
    fi
    case "$DIST_ARCH" in
        'amd64' | 'athlon' | 'i386' | 'i486' | 'i586' | 'i686' | 'x86_64')
            DIST_ARCH="x86"
            ;;
        'ia32' | 'ia64' | 'ia64n' | 'ia64w')
            DIST_ARCH="ia"
            ;;
        'ip27')
            DIST_ARCH="mips"
            ;;
        'power' | 'powerpc' | 'power_pc' | 'ppc64')
            DIST_ARCH="ppc"
            ;;
        'pa_risc' | 'pa-risc')
            DIST_ARCH="parisc"
            ;;
        'sun4u' | 'sparcv9')
            DIST_ARCH="sparc"
            ;;
        '9000/800')
            DIST_ARCH="parisc"
            ;;
    esac
fi

# OSX always places Java in the same location so we can reliably set JAVA_HOME
if [ "$DIST_OS" = "macosx" ]; then
    if [ -z "$JAVA_HOME" ]; then
        JAVA_HOME="/Library/Java/Home"; export JAVA_HOME
    fi
fi

outputFile() {
    if [ -f "${PRODUCT}" ]; then
        echo "  ${PRODUCT} (Found but not executable.)";
    else
        echo "  ${PRODUCT}"
    fi
}

# Decide on the wrapper binary to use.
# If a 32-bit wrapper binary exists then it will work on 32 or 64 bit
#  platforms, if the 64-bit binary exists then the distribution most
#  likely wants to use long names.  Otherwise, look for the default.
WRAPPER_TEST_CMD="$WRAPPER_CMD-$DIST_OS-$DIST_ARCH-32"
if [ -x "$WRAPPER_TEST_CMD" ]; then
    WRAPPER_CMD="$WRAPPER_TEST_CMD"
else
    WRAPPER_TEST_CMD="$WRAPPER_CMD-$DIST_OS-$DIST_ARCH-64"
    if [ -x "$WRAPPER_TEST_CMD" ];  then
        WRAPPER_CMD="$WRAPPER_TEST_CMD"
    else
        if [ ! -x "$WRAPPER_CMD" ];  then
            echo "Unable to locate any of the following binaries:"
            outputFile "$WRAPPER_CMD-$DIST_OS-$DIST_ARCH-32"ls
            outputFile "$WRAPPER_CMD-$DIST_OS-$DIST_ARCH-64"
            outputFile "$WRAPPER_CMD"
            doExit 1
        fi
    fi
fi

# Build the nice clause
if [ "X$PRIORITY" = "X" ]; then
    CMDNICE=""
else
    CMDNICE="nice -$PRIORITY"
fi

# Build the anchor file clause.
if [ "X$IGNORE_SIGNALS" = "X" ]; then
   ANCHORPROP=
   IGNOREPROP=
else
   ANCHORPROP=wrapper.anchorfile=\"$ANCHORFILE\"
   IGNOREPROP=wrapper.ignore_signals=TRUE
fi

# Build the status file clause.
if [ "X$DETAIL_STATUS" = "X" ]; then
   STATUSPROP=
else
   STATUSPROP="wrapper.statusfile=\"$STATUSFILE\" wrapper.java.statusfile=\"$JAVASTATUSFILE\""
fi

# Build the lock file clause.  Only create a lock file if the lock directory exists on this platform.
LOCKPROP=
if [ -d $LOCKDIR ]; then
    if [ -w $LOCKDIR ]; then
        LOCKPROP=wrapper.lockfile=\"$LOCKFILE\"
    fi
fi

#----------------------------------------------------------------------
checkUser() {
    # ${PRODUCT} touchLock flag
    # ${INSTANCE} command

    # Check the configured user.  If necessary rerun this script as the desired user.
    if [ "X$RUN_AS_USER" != "X" ]; then
        # Resolve the location of the 'id' command
        IDEXE="/usr/xpg4/bin/id"
        if [ ! -x "$IDEXE" ]; then
            IDEXE="/usr/bin/id"
            if [ ! -x "$IDEXE" ]
            then
                echo "Unable to locate 'id'."
                echo "Please report this message along with the location of the command on your system."
                doExit 1
            fi
        fi
    
        if [ "`$IDEXE -u -n`" = "$RUN_AS_USER" ]; then
            # Already running as the configured user.  Avoid password prompts by not calling su.
            RUN_AS_USER=""
        fi
    fi
    if [ "X$RUN_AS_USER" != "X" ]; then
        # If LOCKPROP and $RUN_AS_USER are defined then the new user will most likely not be
        # able to create the lock file.  The Wrapper will be able to update this file once it
        # is created but will not be able to delete it on shutdown.  If ${INSTANCE} is defined then
        # the lock file should be created for the current command
        if [ "X$LOCKPROP" != "X" ]; then
            if [ "X${PRODUCT}" != "X" ]; then
                # Resolve the primary group 
                RUN_AS_GROUP=`groups $RUN_AS_USER | awk '{print $3}' | tail -1`
                if [ "X$RUN_AS_GROUP" = "X" ];  then
                    RUN_AS_GROUP=$RUN_AS_USER
                fi
                touch $LOCKFILE
                chown $RUN_AS_USER:$RUN_AS_GROUP $LOCKFILE
            fi
        fi

        # Still want to change users, recurse.  This means that the user will only be
        #  prompted for a password once. Variables shifted by 1
        # 
        # Use "runuser" if this exists.  runuser should be used on RedHat in preference to su.
        #
        if test -f "/sbin/runuser"; then
            /sbin/runuser - $RUN_AS_USER -c "\"$REALPATH\" ${INSTANCE}"
        else
            su - $RUN_AS_USER -c "\"$REALPATH\" ${INSTANCE}"
        fi

        # Now that we are the original user again, we may need to clean up the lock file.
        if [ "X$LOCKPROP" != "X" ]; then
            getPid
            if [ "X$pid" = "X" ]; then
                # Wrapper is not running so make sure the lock file is deleted.
                if [ -f "$LOCKFILE" ]; then
                    rm "$LOCKFILE"
                fi
            fi
        fi

        doExit 0
    fi
}

#----------------------------------------------------------------------
getPid() {
    #log "getPid in..." 
    loadPid
    if [ "X$pid" = "X" ]; then
       findPid
    fi
    #log "getPid : ${pid}"
    storePid
    #log "getPid ...out"
}

#----------------------------------------------------------------------
readPid() {
   _PID_FROM_FILE=0
    if [ -f "$PIDFILE" ] && [ -r "$PIDFILE" ]; then
       pid=`cat "$PIDFILE"`
       log "Pid is [$pid]" 
       _PID_FROM_FILE=1
    fi   
}       
       
#----------------------------------------------------------------------
loadPid() {
    pid=""
    _PID_FROM_FILE=0
    if [ -f "$PIDFILE" ] && [ -r "$PIDFILE" ]; then
       pid=`cat "$PIDFILE"`
       log "Pid file found containing [$pid]" 
       pid=`$PSEXE -p $pid | grep $pid | grep -v grep | awk '{print $1}' | tail -1`
       if [ ! "X$pid" = "X" ];  then
          log "pid matched to running process ${pid}" 
          _PID_FROM_FILE=1
       else
          log "pid is stale" 
          pid=""
          rm -f "$PIDFILE" 
          log "deleting pid file"   
       fi
    fi   
}    

#----------------------------------------------------------------------
findPid() {
    if [ "X$pid" = "X" ]; then
        _PID_FROM_FILE=0
        # Check to see if there is a running instance and parse its pid.          
        ps -o pid -o pgid -o args -Al | grep ${WRAPPER_CMD} | while read GREP_LINE; do
           A_PID=`echo ${GREP_LINE} | awk '{print $1}'`
           CMD=`echo ${GREP_LINE} | awk '{print $3}'`
           if [ "X${CMD}" = "X${WRAPPER_CMD}" ]; then
              pid="${A_PID}"
              echo "${pid}" > ${PIDFILE}
              log "writting ${pid} into pid file"
              _PID_FROM_FILE=1
           fi
        done 
        readPid
    fi
}

#----------------------------------------------------------------------
storePid() {

    if [ "X$pid" = "X" ];  then
        # Process is gone so remove the pid file.
        if [ -f "$PIDFILE" ]; then
           rm -f "$PIDFILE" 
           log "deleting pid file"
        fi
    else    
        if [ ${_PID_FROM_FILE} -eq 0 ]; then
           echo "${pid}" > ${PIDFILE}
           log "writting ${pid} into pid file"
        fi
    fi    
}

#----------------------------------------------------------------------
testPid() {
    if [ ! "X$pid" = "X" ];  then
       pid=`$PSEXE -p $pid | grep $pid | grep -v grep | awk '{print $1}' | tail -1`
       if [ "X$pid" = "X" ];  then
          # Process is gone so remove the pid file.
          if [ -f "$PIDFILE" ]; then
             rm -f "$PIDFILE"
             log "Process terminated. Deleting pid file..."
          fi   
       fi
    fi
}

#----------------------------------------------------------------------
getstatus() {
    STATUS=
    if [ -f "$STATUSFILE" ]; then
        if [ -r "$STATUSFILE" ];  then
            STATUS=`cat "$STATUSFILE"`
        fi
    fi
    if [ "X$STATUS" = "X" ]; then
        STATUS="Unknown"
    fi
    
    JAVASTATUS=
    if [ -f "$JAVASTATUSFILE" ]; then
        if [ -r "$JAVASTATUSFILE" ]; then
            JAVASTATUS=`cat "$JAVASTATUSFILE"`
        fi
    fi
    if [ "X$JAVASTATUS" = "X" ]; then
        JAVASTATUS="Unknown"
    fi
}

#----------------------------------------------------------------------
report() {
   echo "The running Wrapper Services are:"
   ps -o pid -o pgid -o args -Al | grep wrapper | while read wrappers; do
     prog=`echo ${wrappers} | awk '{print $3}'`
     temp=`echo ${prog} | tr '/' ' '`
     binary=`echo ${temp} | awk '{print $NF}'`
     if [ "X${binary}" = "Xwrapper" ]; then
       preText=`echo ${temp} | awk '{print $1}'`
       if [ "${preText}" = "apps" ]; then
          program=`echo ${temp} | awk '{print $2}'`
          instance=`echo ${temp} | awk '{print $4}'`
       elif [ "${preText}" = "export" ]; then
          program=`echo ${temp} | awk '{print $3}'`
          instance=`echo ${temp} | awk '{print $5}'`
       fi
       pid=`echo ${wrappers} | awk '{print $1}'`
       pgid=`echo ${wrappers} | awk '{print $2}'`
       if [ "X${prog}" = "X/apps/${program}/admin/${instance}/${binary}" ]; then
         if [ "X${program}" != "X" ]; then
           if [ "X${instance}" != "X" ]; then
             psResults=`ps -g $pgid -o pid -o args | grep java`
             ps -g $pgid -o pid -o args | grep java | while read javas; do
               cpid=`echo ${javas} | awk '{print $1}'`
               echo "  ${program} ${instance} (${binary} pid=${pid}, java pid=${cpid})"
             done
             if [ "X${psResults}" = "X" ]; then
               echo "   [${cpid}]"
               echo "  ${program} ${instance} (${binary} pid=${pid}, java pid=[NONE])"
             fi
           fi
         fi
       elif [ "X${prog}" = "X/apps/${program}/admin/${instance}/${binary}" ]; then
         if [ "X${program}" != "X" ]; then
           if [ "X${instance}" != "X" ]; then
             psResults=`ps -g $pgid -o pid -o args | grep java`
             ps -g $pgid -o pid -o args | grep java | while read javas; do
               cpid=`echo ${javas} | awk '{print $1}'`
               echo "  ${program} ${instance} (${binary} pid=${pid}, java pid=${cpid})"
             done
             if [ "X${psResults}" = "X" ]; then
               echo "   [${cpid}]"
               echo "  ${program} ${instance} (${binary} pid=${pid}, java pid=[NONE])"
             fi
           fi
         fi
       else
         psResults=`ps -g $pgid -o pid -o args | grep java`
         ps -g $pgid -o pid -o args | grep java | while read javas; do
           cpid=`echo ${javas} | awk '{print $1}'`
           echo "  Unknown_service (${prog} pid=${pid}, java pid=${cpid})"
         done
         if [ "X${psResults}" = "X" ]; then
           echo "  Unknown_service (${prog} pid=${pid}, java pid=[NONE])"
         fi
       fi
     fi
   done
   echo ""
}

#----------------------------------------------------------------------
console() {
    echo "Running $APP_LONG_NAME..."
    getPid
    if [ "X$pid" = "X" ]; then
        setDebugIfRequired
        setOptionsIfRequired ${ARGS}
        # The string passed to eval must handles spaces in paths correctly.
        COMMAND_LINE="$CMDNICE \"$WRAPPER_CMD\" \"$WRAPPER_CONF\" wrapper.syslog.ident=\"$APP_NAME\" wrapper.pidfile=\"$PIDFILE\" wrapper.name=\"$APP_NAME\" wrapper.displayname=\"$APP_LONG_NAME\" $ANCHORPROP $STATUSPROP $LOCKPROP"
        eval $COMMAND_LINE
    else
        echo "$APP_LONG_NAME is already running."
        doExit 1
    fi
}
 
#----------------------------------------------------------------------
start() {
    echo "Starting $APP_LONG_NAME..."
    getPid
    if [ "X$pid" = "X" ]; then
        log "---> starting the process"
        # The string passed to eval must handles spaces in paths correctly.
        setDebugIfRequired
        setOptionsIfRequired ${ARGS}
        COMMAND_LINE="$CMDNICE \"$WRAPPER_CMD\" \"$WRAPPER_CONF\" wrapper.syslog.ident=\"$APP_NAME\" wrapper.pidfile=\"$PIDFILE\" wrapper.name=\"$APP_NAME\" wrapper.displayname=\"$APP_LONG_NAME\" wrapper.daemonize=TRUE $ANCHORPROP $IGNOREPROP $STATUSPROP $LOCKPROP"
        eval $COMMAND_LINE
        report
    else
        echo "$APP_LONG_NAME is already running."
        doExit 1
    fi

    # Sleep for a few seconds to allow for intialization if required 
    #  then test to make sure we're still running.
    #
    i=0
    while [ $i -lt $WAIT_AFTER_STARTUP ]; do
        sleep 1
        echo -n "."
        i=`expr $i + 1`
    done
    if [ $WAIT_AFTER_STARTUP -gt 0 ]; then
        readPid
        if [ "X$pid" = "X" ]; then
            echo " WARNING: $APP_LONG_NAME may have failed to start."
            doExit 1
        else
            echo " running ($pid)."
        fi
    else 
        readPid
        echo ""
    fi
}
 
#---------------------------------------------------------------------- 
stopit() {
    echo "Stopping $APP_LONG_NAME..."
    getPid
    if [ "X$pid" = "X" ]; then
        echo "$APP_LONG_NAME was not running."
    else
        if [ "X$IGNORE_SIGNALS" = "X" ];  then
            # Running so try to stop it.
            kill $pid
            if [ $? -ne 0 ];  then
                # An explanation for the failure should have been given
                echo "Unable to stop $APP_LONG_NAME."
                doExit 1
            fi
        else
            rm -f "$ANCHORFILE"
            if [ -f "$ANCHORFILE" ];  then
                # An explanation for the failure should have been given
                echo "Unable to stop $APP_LONG_NAME."
                doExit 1
            fi
        fi

        # We can not predict how long it will take for the wrapper to
        #  actually stop as it depends on settings in wrapper.conf.
        #  Loop until it does.
        savepid=$pid
        CNT=0
        TOTCNT=0
        while [ "X$pid" != "X" ]; do
            pid=$savepid
            # Show a waiting message every 5 seconds.
            if [ "$CNT" -lt "5" ]
            then
                CNT=`expr $CNT + 1`
            else
                echo "Waiting for $APP_LONG_NAME to exit..."
                CNT=0
            fi
            TOTCNT=`expr $TOTCNT + 1`

            sleep 1

            testPid
        done

        pid=$savepid
        testPid
        if [ "X$pid" != "X" ]; then
            echo "Failed to stop $APP_LONG_NAME."
            doExit 1
        else
            echo "Stopped $APP_LONG_NAME."
        fi
    fi
}

#----------------------------------------------------------------------
status() {
    getPid
    if [ "X$pid" = "X" ]; then
        echo "$APP_LONG_NAME is not running."
        doExit 1
    else
        if [ "X$DETAIL_STATUS" = "X" ]; then
            echo "$APP_LONG_NAME is running (PID:$pid)."
        else
            getstatus
            echo "$APP_LONG_NAME is running (PID:$pid, Wrapper:$STATUS, Java:$JAVASTATUS)"
        fi
        doExit 0
    fi
}

#----------------------------------------------------------------------
dump() {
    echo "Dumping $APP_LONG_NAME..."
    getpPid
    if [ "X$pid" = "X" ]; then
        echo "$APP_LONG_NAME was not running."
    else
        kill -3 $pid

        if [ $? -ne 0 ]; then
            echo "Failed to dump $APP_LONG_NAME."
            doExit 1
        else
            echo "Dumped $APP_LONG_NAME."
        fi
    fi
}

# Used by HP-UX init scripts.
#----------------------------------------------------------------------
startmsg() {
    getPid
    if [ "X$pid" = "X" ];  then
        echo "Starting $APP_LONG_NAME... (Wrapper:Stopped)"
    else
        if [ "X$DETAIL_STATUS" = "X" ]; then
            echo "Starting $APP_LONG_NAME... (Wrapper:Running)"
        else
            getstatus
            echo "Starting $APP_LONG_NAME... (Wrapper:$STATUS, Java:$JAVASTATUS)"
        fi
    fi
}

#----------------------------------------------------------------------
setDebugIfRequired() {
   if [ -f ${DEBUG_CONF} ]; then
      rm -f ${DEBUG_CONF} 2>&1 > /dev/null
   fi
   if [ $_DEBUG_FLAG -gt 0 ]; then
      _DEBUG_LINE="wrapper.java.additional.1=-agentlib:jdwp=transport=dt_socket,address=${PORT},server=y"
      if [ $_DEBUG_FLAG -eq 1 ]; then
         _DEBUG_LINE="${_DEBUG_LINE},suspend=n"
      else
         _DEBUG_LINE="${_DEBUG_LINE},suspend=y"
      fi
      
      cp -f ${WRAPPER_CONF} ${DEBUG_CONF} 2>&1 > /dev/null
      echo "" >> ${DEBUG_CONF}
      echo "${_DEBUG_LINE}" >> ${DEBUG_CONF}
      if [ -f ${DEBUG_CONF} ]; then
         WRAPPER_CONF="${DEBUG_CONF}"
      fi
   fi
}

#----------------------------------------------------------------------
setOptionsIfRequired() {
   _ARG_INDEX=`cat ${WRAPPER_CONF} | grep ^wrapper.app.parameter | wc -l | awk '{print $1}'`
   _JVM_INDEX=`cat ${WRAPPER_CONF} | grep ^wrapper.java.additional | wc -l | awk '{print $1}'`
   # TEG 20/05/2019: Had to remove this as we are passing in arguments that are not related to debug
   # and this automatically assumes that any args passed in mean we are in debug mode
   #if [ $# -gt 0 ]; then
   #   if [ "${WRAPPER_CONF}" != "${DEBUG_CONF}" ]; then
   #      cp -f ${WRAPPER_CONF} ${DEBUG_CONF} 2>&1 > /dev/null
   #      WRAPPER_CONF="${DEBUG_CONF}"
   #   fi
   #fi   
   while [ $# -gt 0 ]; do
      _ARG_INDEX=`expr ${_ARG_INDEX} + 1`
      _ARG="$1"
      _NEXT_ARG="$2"
      _IS_JVM_FLAG=`isJVMFlag ${_ARG}`
      _IS_ARG1_FLAG=`isValueFlag ${_ARG}`
      _IS_ARG2_FLAG=`isValueFlag ${_NEXT_ARG}`
      if [ "${_IS_JVM_FLAG}" = "1" ]; then
         _JVM_INDEX=`expr ${_JVM_INDEX} + 1`
         _OPTION_LINE="wrapper.java.additional.${_JVM_INDEX}=${_ARG}"
         _ARG_INDEX=`expr ${_ARG_INDEX} - 1`
         shift 1
#      elif [ "${_IS_ARG1_FLAG}" = "0" ]; then
#         _OPTION_LINE="wrapper.app.parameter.${_ARG_INDEX}=${_ARG}"
#         shift 1
#      elif [ "${_IS_ARG2_FLAG}" = "0" ]; then
#         _OPTION_LINE="wrapper.app.parameter.${_ARG_INDEX}=${_ARG} ${_NEXT_ARG}"
#         shift 2
      else
         _OPTION_LINE="wrapper.app.parameter.${_ARG_INDEX}=${_ARG}"
         shift 1
      fi   
      echo "${_OPTION_LINE}" >> ${WRAPPER_CONF}
      echo "${_OPTION_LINE}"
   done
   echo "Additional Startup parameters:"
   echo "WRAPPER_CONF =${WRAPPER_CONF}"
   
}

#----------------------------------------------------------------------
isValueFlag() {
   if [ $# -gt 0 ]; then
      _TEST=`echo "$1" | grep "^-"`
      if [ "${_TEST}" = "" ]; then
         echo "0"
      else 
         echo "1"
      fi
   else 
      echo "0"
   fi
}

isJVMFlag() {
   if [ $# -gt 0 ]; then
      _TEST=`echo "$1" | grep "^-D"`
      if [ "${_TEST}" = "" ]; then
         echo "0"
      else 
         echo "1"
      fi
   else 
      echo "0"
   fi
}

# Used by HP-UX init scripts.
#----------------------------------------------------------------------
stopmsg() {
    getPid
    if [ "X$pid" = "X" ]; then
        echo "Stopping $APP_LONG_NAME... (Wrapper:Stopped)"
    else
        if [ "X$DETAIL_STATUS" = "X" ];  then
            echo "Stopping $APP_LONG_NAME... (Wrapper:Running)"
        else
            getstatus
            echo "Stopping $APP_LONG_NAME... (Wrapper:$STATUS, Java:$JAVASTATUS)"
        fi
    fi
}

#----------------------------------------------------------------------
case "${MODE}" in

    'console')
        checkUser touchlock ${MODE}
        console
        ;;

    'start')
        checkUser touchlock ${MODE}
        start
        ;;

    'stop')
        checkUser "" ${MODE}
        stopit
        ;;

    'restart')
        checkUser touchlock ${MODE}
        stopit
        start
        ;;

    'status')
        checkUser "" ${MODE}
        status
        ;;

    'dump')
        checkUser "" ${MODE}
        dump
        ;;

    'start_msg')
        checkUser "" ${MODE}
        startmsg
        ;;

    'stop_msg')
        checkUser "" ${MODE}
        stopmsg
        ;;

    'check')
        getPid
        if [ "X$pid" != "X" ] ;  then
           echo "running"
        fi
        ;;

    *)
        echo "Usage: $0 { console | start | stop | restart | status | dump | check }"
        doExit 1
        ;;
esac

doExit 0
